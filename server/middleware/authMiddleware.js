const jwt = require('jsonwebtoken');
require('dotenv').config();

// eslint-disable-next-line consistent-return
const authMiddleware = (req, res, next) => {
  const {
    authorization,
  } = req.headers;

  if (!authorization) {
    return res.status(401).json({ message: 'Please, provide authorization header', status: 401 });
  }

  const [, token] = authorization.split(' ');

  if (!token) {
    return res.status(401).json({ message: 'Please, include token to request', status: 401 });
  }

  try {
    const tokenPayload = jwt.verify(token, `${process.env.SECRET_KEY}`);
    req.user = {
      userId: tokenPayload.userId,
      role: tokenPayload.role,
      email: tokenPayload.email,
      createdDate: tokenPayload.createdDate,
      name: tokenPayload.name,
    };
    next();
  } catch (err) {
    return res.status(401).json({ message: err.message, status: 401 });
  }
};

module.exports = {
  authMiddleware,
};
