const { Truck, TruckJoiSchema } = require('../models/Trucks');
const { saveTruck, updateTruck } = require('../services/truckService');

const getAllUsersTrucks = async (req, res) => {
  if (req.user.role !== 'DRIVER') throw new Error('You are not a Driver');
  const trucks = await Truck.find({ created_by: req.user.userId });

  return res.status(200).send({
    trucks,
    status: 200,
    total_count: trucks.length,
  });
};

const postTruck = async (req, res) => {
  if (req.user.role !== 'DRIVER') throw new Error('You are not a driver');
  const { type } = req.body;

  if (!type) {
    return res.status(400).send({
      message: 'Please specify type of truck',
    });
  }

  const truck = await saveTruck({
    userId: req.user.userId,
    type,
  });

  if (!truck) {
    return res.status(400).send({
      message: 'Something went wrong so we could post you truck',
    });
  }

  return res.status(200).send({
    message: 'Truck created successfully',
    status: 200,
    truck,
  });
};

const getTruckById = async (req, res) => {
  if (req.user.role !== 'DRIVER') throw new Error('You are not a Driver');
  const { truckId } = req.params;

  if (!truckId) {
    return res.status(400).send({
      message: 'Please specify correct truck id',
    });
  }

  const truck = await Truck.findOne({ _id: truckId, created_by: req.user.userId });

  if (!truck) {
    return res.status(400).send({
      message: 'We couldn\'t find truck with such an ID or it\'s not your truck',
    });
  }

  return res.status(200).send({
    truck,
  });
};

const deleteTruckById = async (req, res) => {
  if (req.user.role !== 'DRIVER') throw new Error('You are not a Driver');
  const { truckId } = req.params;

  const truck = await Truck.findOneAndDelete({
    _id: truckId,
    created_by: req.user.userId,
    assigned_to: { $eq: 'null' },
  });
  if (!truck) {
    return res.status(400).send({
      message: 'You can\'t delete already assigned truck or something went wrong',
    });
  }

  return res.status(200).send({
    message: 'Truck deleted successfully',
    status: 200,
  });
};

const assignTruck = async (req, res) => {
  if (req.user.role !== 'DRIVER') throw new Error('You are not a driver!!');

  const { truckId } = req.params;
  let alreadyAssign = false;

  if (!truckId) {
    return res.status(400).send({
      message: 'Please specify truck ID',
    });
  }

  const truck = await Truck.find({ created_by: req.user.userId });

  if (!truck) {
    return res.status(400).send({
      message: 'We cannot find truck with such an ID',
    });
  }

  // eslint-disable-next-line no-plusplus
  for (let i = 0; i < truck.length; i++) {
    if (truck[i].assigned_to !== 'null') {
      alreadyAssign = true;
      break;
    }
  }

  if (alreadyAssign) {
    return res.status(400).send({
      message: 'You cannot assign more than one truck',
    });
  }

  const updatedTruck = await Truck.findOneAndUpdate({
    _id: truckId,
    created_by: req.user.userId,
  }, {
    $set: { assigned_to: req.user.userId },
  });

  if (!updatedTruck) {
    return res.status(400).send({
      message: 'Something went wrong',
    });
  }

  return res.status(200).send({
    message: 'Truck assigned successfully',
    status: 200,
  });
};

const updateTruckInfo = async (req, res) => {
  if (req.user.role !== 'DRIVER') throw new Error('You are not a Driver');
  const { type } = req.body;
  TruckJoiSchema.validate({ type });
  const { truckId } = req.params;

  if (!type || !truckId) {
    return res.status(400).send({
      message: 'Please specify type parameter or truck ID',
    });
  }

  const updatedTruck = await updateTruck({
    type,
    truckId,
    userId: req.user.userId,
  });

  if (!updatedTruck) {
    return res.status(400).send({
      message: 'We cannot find such a truck or we cannot update already assigned truck',
    });
  }

  return res.status(200).send({
    message: 'Truck details changed successfully',
    status: 200,
    truck: updatedTruck,
  });
};

module.exports = {
  getAllUsersTrucks,
  postTruck,
  getTruckById,
  deleteTruckById,
  assignTruck,
  updateTruckInfo,
};
