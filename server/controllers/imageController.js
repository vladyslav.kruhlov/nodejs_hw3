const multer = require('multer');
const path = require('path');
const fs = require('fs');

const storageConfig = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, path.join('server/uploads'));
  },
  filename: (req, file, cb) => {
    cb(null, req.user.email);
  },
});

const upload = multer({ storage: storageConfig });

const uploadImage = (req, res) => {
  const { file } = req.body;

  fs.writeFile(path.join('server', 'uploads', req.user.email), file, (err) => {
    if (err) {
      return res.status(400).send({
        message: 'We cannot upload your file',
      });
    }

    return res.status(200).send({
      message: 'Image successfully uploaded',
      status: 200,
    });
  });
};

const getImage = (req, res) => {
  fs.exists(path.join('server', 'uploads', req.user.email), (exists) => {
    if (exists) {
      fs.readFile(path.join('server', 'uploads', req.user.email), 'utf-8', (err, files) => {
        if (err) {
          return res.status(400).send({
            message: 'We cannot get your image',
            err,
          });
        }

        return res.status(200).send({
          image: files,
          mimetype: files.mimetype,
          status: 200,
        });
      });
    }
  });
};

module.exports = {
  uploadImage,
  upload,
  getImage,
};
