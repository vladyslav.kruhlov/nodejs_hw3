const { Chat } = require('../models/Chats');

const getChatInfoById = async (req, res) => {
  const { chatID } = req.params;

  const chat = await Chat.findOne({
    _id: chatID,
  });

  if (!chat) {
    return res.status(400).send({
      message: 'We cannot get messages',
    });
  }

  return res.status(200).send({
    messages: chat.messages,
    status: 200,
  });
};

module.exports = {
  getChatInfoById,
};
