const {
  Load,
  LoadJoiSchema,
} = require('../models/Loads');
const { saveLoad } = require('../services/loadsService');
const { getAllAssignedTruck } = require('../services/truckService');
const { Truck } = require('../models/Trucks');
const { User } = require('../models/Users');
const { changeState } = require('../services/loadsService');
const { SaveChat } = require('../services/chatService');

const getAllUsersLoads = async (req, res) => {
  if (req.user.role === 'DRIVER') {
    const loads = await Load.find({
      assigned_to: req.user.userId,
    });

    return res.status(200).send({
      loads,
      status: 200,
    });
  }

  const {
    status,
    limit,
    offset,
    page,
  } = req.query;
  const queries = {};
  queries.limit = limit || 0;
  queries.skip = offset || 0;
  queries.skip = page || 0;

  if (status) {
    queries.status = status;
  }

  const loads = await Load.find({ created_by: req.user.userId }, null, { ...queries });

  return res.status(200).send({
    loads,
    total_count: loads.length,
    status: 200,
  });
};

const addLoad = async (req, res) => {
  if (req.user.role !== 'SHIPPER') throw new Error('You are not a Shipper!!!');
  const {
    name,
    payload,
    // eslint-disable-next-line camelcase
    pickup_address,
    // eslint-disable-next-line camelcase
    delivery_address,
    dimensions,
    // eslint-disable-next-line camelcase
    created_date,
  } = req.body;

  // eslint-disable-next-line camelcase
  if (!name || !payload || !pickup_address || !delivery_address || !dimensions) {
    return res.status(400).send({
      message: 'You missed one of required parameters',
    });
  }

  const load = await saveLoad({
    userId: req.user.userId,
    name,
    payload,
    // eslint-disable-next-line camelcase
    pickupAddress: pickup_address,
    // eslint-disable-next-line camelcase
    deliveryAddress: delivery_address,
    dimensions,
    // eslint-disable-next-line camelcase
    created_date,
  });

  if (!load) {
    return res.status(400).send({
      message: 'Something went wrong',
    });
  }

  return res.status(200)
    .send({
      message: 'Load created successfully',
      status: 200,
      load,
    });
};

const getActiveLoad = async (req, res) => {
  if (req.user.role !== 'DRIVER') throw new Error('You are not a Driver!!!');
  const activeLoads = await Load.findOne({
    assigned_to: req.user.userId,
    status: { $ne: 'SHIPPED' },
  });

  return res.status(200).send({
    load: activeLoads,
    status: 200,
  });
};

const changeActiveLoadState = async (req, res) => {
  if (req.user.role !== 'DRIVER') throw new Error('You are not a Driver');
  const load = await Load.findOne({
    assigned_to: req.user.userId,
    status: { $ne: 'SHIPPED' },
  });

  if (!load) {
    return res.status(400).send({
      message: 'You don\'t have an active load',
    });
  }

  let changedLoad;

  const newState = changeState(load.state);

  if (newState === 'Arrive to delivery') {
    changedLoad = await Load.findOneAndUpdate({
      assigned_to: req.user.userId,
      status: { $ne: 'SHIPPED' },
    }, {
      $set: {
        state: newState,
        status: 'SHIPPED',
      },
    });
    await Truck.findOneAndUpdate({
      assigned_to: req.user.userId,
    }, {
      $set: {
        status: 'IS',
        assigned_to: 'null',
      },
    });
  } else {
    changedLoad = await Load.findOneAndUpdate({
      assigned_to: req.user.userId,
      status: { $ne: 'SHIPPED' },
    }, {
      $set: {
        state: newState,
      },
    });
  }

  if (!changedLoad) {
    return res.status(400)
      .send({
        message: 'We cannot changed state. Try again later',
      });
  }

  return res.status(200).send({
    message: `Load state changed to '${newState}'`,
    status: 200,
    state: newState,
  });
};

const getLoadById = async (req, res) => {
  const { loadId } = req.params;

  const load = await Load.findOne({
    _id: loadId,
    created_by: req.user.userId,
  });
  return res.status(200)
    .send({
      load,
    });
};

const uploadLoad = async (req, res) => {
  const { loadId } = req.params;
  const {
    name,
    payload,
    // eslint-disable-next-line camelcase
    pickup_address,
    // eslint-disable-next-line camelcase
    delivery_address,
    dimensions,
  } = req.body;

  if (name) LoadJoiSchema.validate({ name });
  if (payload) LoadJoiSchema.validate({ payload });
  // eslint-disable-next-line camelcase
  if (pickup_address) LoadJoiSchema.validate({ pickup_address });
  // eslint-disable-next-line camelcase
  if (delivery_address) LoadJoiSchema.validate({ delivery_address });
  if (dimensions) LoadJoiSchema.validate({ dimensions });

  const updatedLoad = await Load.findOneAndUpdate({
    _id: loadId,
  }, {
    $set: req.body,
  });

  if (!updatedLoad) {
    return res.status(400).send({
      message: 'Something went wrong we cannot update your load',
    });
  }

  return res.status(200).send({
    status: 200,
    message: 'Load details changed successfully',
  });
};

const deleteLoad = async (req, res) => {
  if (req.user.role !== 'SHIPPER') throw new Error('You are not a Shipper');
  const { loadId } = req.params;

  if (!loadId) {
    return res.status(400)
      .send({
        message: 'Please specify load ID',
      });
  }

  const load = await Load.findOneAndDelete({
    _id: loadId,
    created_by: req.user.userId,
    status: { $eq: 'NEW' },
  });

  if (!load) {
    return res.status(400).send({
      message: 'We cannot delete load with status different from "NEW".',
    });
  }

  return res.status(200).send({
    message: 'Load deleted successfully',
    status: 200,
  });
};

// eslint-disable-next-line consistent-return
const postLoadById = async (req, res) => {
  if (req.user.role !== 'SHIPPER') throw new Error('You are not a Shipper');
  const { loadId } = req.params;

  if (!loadId) {
    return res.status(400).send({
      message: 'Please specify load ID',
    });
  }

  const load = await Load.findOneAndUpdate({
    _id: loadId,
    created_by: req.user.userId,
  }, {
    $set: { status: 'POSTED' },
  });

  if (!load || load.status !== 'NEW') {
    return res.status(400).send({
      message: 'Something went wrong we cannot post your load',
    });
  }

  const truck = await getAllAssignedTruck({
    payload: load.payload,
    dimensions: load.dimensions,
  });

  if (!truck) {
    await Load.findOneAndUpdate({
      _id: loadId,
      created_by: req.user.userId,
    }, {
      $set: { status: 'NEW' },
    });

    return res.status(400).send({
      message: 'We cannot find driver',
    });
  }

  await Truck.findOneAndUpdate({
    // eslint-disable-next-line no-underscore-dangle
    _id: truck._id,
  }, {
    $set: { status: 'OL' },
  });

  const chat = await SaveChat(truck.assigned_to, req.user.userId);

  const assignedUser = await User.findOneAndUpdate({
    _id: truck.assigned_to,
  }, {
    $push: {
      chats: {
        // eslint-disable-next-line no-underscore-dangle
        id: chat._id,
        friend_name: req.user.name,
        load: loadId,
      },
    },
  });

  await User.updateOne({
    _id: req.user.userId,
  }, {
    $push: {
      chats: {
        // eslint-disable-next-line no-underscore-dangle
        id: chat._id,
        friend_name: assignedUser.name,
        load: loadId,
      },
    },
  });

  await Load.findOneAndUpdate({
    _id: loadId,
    created_by: req.user.userId,
  }, {
    $set: {
      logs: [
        {
          message: `Load assigned to driver with id: ${truck.assigned_to}`,
          time: new Date(),
        },
      ],
      status: 'ASSIGNED',
      state: 'En route to Pick Up',
      assigned_to: truck.assigned_to,
    },
  });

  return res.status(200).send({
    message: 'Load posted successfully',
    driver_found: true,
    status: 200,
  });
};

const getFullLoadInfo = async (req, res) => {
  if (req.user.role !== 'SHIPPER') throw new Error('You are not a Shipper');
  const { loadId } = req.params;

  if (!loadId) {
    return res.status(400).send({
      message: 'Please specify load ID',
    });
  }

  const load = await Load.findOne({
    _id: loadId,
    created_by: req.user.userId,
    assigned_to: { $exists: true },
  });

  if (!load) {
    return res.status(400).send({
      message: 'We cannot find load with such and ID',
    });
  }

  const truck = await Truck.findOne({
    assigned_to: load.assigned_to,
  });

  if (!truck) {
    return res.status(400).send({
      message: 'wrong',
    });
  }

  return res.status(200).send({
    load,
    truck,
  });
};

module.exports = {
  getAllUsersLoads,
  addLoad,
  getActiveLoad,
  changeActiveLoadState,
  getLoadById,
  uploadLoad,
  deleteLoad,
  postLoadById,
  getFullLoadInfo,
};
