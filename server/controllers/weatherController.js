const axios = require('axios');

const getWeather = (req, res) => {
  const { lon, lat } = req.body;

  return axios(`https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&appid=6d3635ed5024117b2199adf1c2388f00`)
    .then((result) => {
      if (result.data) {
        return res.status(200).send({
          data: result.data,
          status: 200,
        });
      }

      return res.status(400).send({
        message: 'We cannot get a weather by this coordinates',
      });
    });
};

module.exports = {
  getWeather,
};
