const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
require('dotenv').config();
const { User, UsersJoiSchema } = require('../models/Users');
const { saveUser } = require('../services/authService');
const { sendEmail } = require('../utils/sendEmail');

const registerUser = async (req, res) => {
  try {
    const {
      email, password, role, name,
    } = req.body;

    if (!email || !password || !role) {
      return res.status(400).send({
        message: 'Specify all require parameters email, password and role',
      });
    }

    await saveUser({
      email, password, role, name: name || 'default',
    });

    return res.status(200).send({
      message: 'Profile created successfully',
      status: 200,
    });
  } catch (err) {
    return res.status(500).send({
      message: 'Something went wrong',
      info: err.message,
    });
  }
};

const loginUser = async (req, res) => {
  const user = await User.findOne({ email: req.body.email });
  UsersJoiSchema.validate({ email: req.body.email, password: req.body.password });
  if (user && await bcrypt.compare(String(req.body.password), String(user.password))) {
    const payload = {
      email: user.email,
      // eslint-disable-next-line no-underscore-dangle
      userId: user._id,
      role: user.role,
      createdDate: user.createdDate,
      name: user.name || 'default',
    };

    const jwtToken = jwt.sign(payload, `${process.env.SECRET_KEY}`);
    return res.status(200).send({
      jwt_token: jwtToken,
      status: 200,
    });
  }
  return res.status(403).json({ message: 'Not authorized' });
};

const forgotPassword = async (req, res) => {
  UsersJoiSchema.validate({ email: req.body.email });
  const user = await User.findOne({ email: req.body.email });

  if (!user) {
    return res.status(404).send({
      message: 'There is no user with that email',
    });
  }

  const message = 'You are receiving this email because you (or someone else) has requested the reset of a password.';

  try {
    await sendEmail({
      email: req.body.email,
      subject: 'Password reset token',
      text: message,
    });

    return res.status(200).send({
      status: 200,
      message: 'Email sent',
    });
  } catch (error) {
    return res.status(500).send({
      message: 'Email could not be sent',
      info: error,
    });
  }
};

const resetPassword = async (req, res) => {
  const { email, newPassword } = req.body;

  UsersJoiSchema.validate({ email, password: newPassword });
  const user = await User.findOneAndUpdate({ email }, {
    $set: {
      password: await bcrypt.hash(newPassword, 10),
    },
  });

  if (!user) {
    return res.status(400).send({
      message: 'User with this email doesn\'t exist',
    });
  }

  return res.status(200).send({
    message: 'Password was successfully changed',
    status: 200,
  });
};

module.exports = {
  loginUser,
  registerUser,
  forgotPassword,
  resetPassword,
};
