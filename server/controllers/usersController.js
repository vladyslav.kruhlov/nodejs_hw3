const bcrypt = require('bcryptjs');
const { User } = require('../models/Users');
const { Load } = require('../models/Loads');
const { Truck } = require('../models/Trucks');

const getMyProfileInfo = async (req, res) => {
  const user = await User.findOne({
    _id: req.user.userId,
  });

  if (!user) {
    return res.status(400).send({
      message: 'We cannot get your profile info',
    });
  }

  return res.status(200).send({
    user: {
      // eslint-disable-next-line no-underscore-dangle
      _id: user._id,
      role: user.role,
      email: user.email,
      createdDate: user.createdDate,
      name: user.name,
      chats: user.chats,
    },
    status: 200,
  });
};

const changeMyPassword = async (req, res) => {
  const user = await User.findById(req.user.userId);
  const {
    oldPassword,
    newPassword,
  } = req.body;

  if (!oldPassword || !newPassword) {
    return res.status(400)
      .send({
        message: 'string',
      });
  }

  if (user && await bcrypt.compare(String(oldPassword), String(user.password))) {
    user.password = await bcrypt.hash(newPassword, 10);

    return user.save()
      .then(() => res.status(200)
        .send({
          message: 'Success',
          status: 200,
        }));
  }

  return res.status(400)
    .send({
      message: 'wrong old password',
    });
};

const deleteAccount = async (req, res) => {
  try {
    const user = await User.findOneAndDelete({ _id: req.user.userId });

    if (!user) {
      return res.status(400)
        .send({
          message: 'We cannot find profile with such an ID',
        });
    }

    if (req.user.role === 'DRIVER') {
      await Truck.deleteMany({
        created_by: req.user.userId,
      });
    }

    if (req.user.role === 'SHIPPER') {
      await Load.deleteMany({
        created_by: req.user.userId,
      });
    }

    return res.status(200)
      .send({
        message: 'Profile deleted successfully',
        status: 200,
      });
  } catch (err) {
    return res.status(500)
      .send({
        message: 'Something went wrong',
        info: err,
      });
  }
};

module.exports = {
  getMyProfileInfo,
  changeMyPassword,
  deleteAccount,
};
