const mongoose = require('mongoose');
const Joi = require('joi');

const TruckJoiSchema = Joi.object({
  type: Joi.string(),
});

const TruckSchema = new mongoose.Schema({
  created_by: {
    type: String,
    required: true,
  },
  assigned_to: {
    type: String,
    required: true,
  },
  type: {
    type: String,
    required: true,
    enum: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'],
  },
  status: {
    type: String,
    required: true,
    enum: ['IS', 'OL'],
  },
  created_date: {
    type: String,
    required: true,
  },
  dimensions: {
    width: {
      type: Number,
      required: true,
    },
    length: {
      type: Number,
      required: true,
    },
    height: {
      type: Number,
      required: true,
    },
  },
  payload_max: {
    type: Number,
    required: true,
  },
});

const Truck = mongoose.model('Truck', TruckSchema);

module.exports = {
  Truck,
  TruckJoiSchema,
};
