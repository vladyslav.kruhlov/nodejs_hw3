const mongoose = require('mongoose');
const Joi = require('joi');

const LoadJoiSchema = Joi.object({
  name: Joi.string(),

  payload: Joi.number(),

  pickup_address: Joi.string(),

  delivery_address: Joi.string(),

  dimensions: {
    width: Joi.number(),

    height: Joi.number(),

    length: Joi.number(),
  },
});

const LoadSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  created_by: {
    type: String,
    required: true,
  },
  logs: [{
    message: {
      type: String,
      required: false,
    },
    time: {
      type: String,
      required: false,
    },
  }],
  assigned_to: {
    type: String,
    required: false,
  },
  status: {
    type: String,
    required: true,
    enum: ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'],
  },
  state: {
    type: String,
    required: false,
    enum: ['En route to Pick Up', 'Arrived to Pick Up', 'En route to delivery', 'Arrived to delivery'],
  },
  dimensions: {
    width: {
      type: Number,
      required: true,
    },
    length: {
      type: Number,
      required: true,
    },
    height: {
      type: Number,
      required: true,
    },
  },
  payload: {
    type: Number,
    required: true,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  created_date: {
    type: String,
    required: true,
  },
});

const Load = mongoose.model('Loads', LoadSchema);

module.exports = {
  Load,
  LoadJoiSchema,
};
