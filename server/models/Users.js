const mongoose = require('mongoose');
const Joi = require('joi');

const UsersJoiSchema = Joi.object({
  email: Joi.string()
    .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } }),

  password: Joi.string()
    .pattern(/^[a-zA-Z0-9]{3,30}$/),

  role: Joi.string(),

  name: Joi.string(),
});

const UserSchema = new mongoose.Schema({
  name: {
    type: String,
    required: false,
  },
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  role: {
    type: String,
    enum: { values: ['DRIVER', 'SHIPPER'] },
    required: true,
  },
  createdDate: {
    type: String,
    required: true,
  },
  chats: {
    type: Array,
    required: false,
    default: [],
  },
});

const User = mongoose.model('User', UserSchema);

module.exports = {
  User,
  UsersJoiSchema,
};
