const mongoose = require('mongoose');

const ChatSchema = new mongoose.Schema({
  users: {
    type: Array,
    required: true,
  },
  messages: {
    type: Array,
    required: false,
  },
});

const Chat = mongoose.model('Chats', ChatSchema);

module.exports = {
  Chat,
};
