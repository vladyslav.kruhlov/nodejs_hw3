const { Chat } = require('../models/Chats');

const SaveChat = async (firstUser, secondUser) => {
  const chat = new Chat({
    users: [firstUser, secondUser],
    messages: [],
  });

  // eslint-disable-next-line no-return-await
  return await chat.save();
};

module.exports = {
  SaveChat,
};
