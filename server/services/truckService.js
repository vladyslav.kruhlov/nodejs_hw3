const { Truck, TruckJoiSchema } = require('../models/Trucks');

function getDimension(type) {
  switch (type) {
    case 'SPRINTER': {
      return {
        dimensions: {
          width: 250,
          length: 300,
          height: 170,
        },
        payload: 1700,
      };
    }
    case 'SMALL STRAIGHT': {
      return {
        dimensions: {
          width: 250,
          length: 500,
          height: 170,
        },
        payload: 2500,
      };
    }
    case 'LARGE STRAIGHT': {
      return {
        dimensions: {
          width: 350,
          length: 700,
          height: 200,
        },
        payload: 4000,
      };
    }
    default: {
      return undefined;
    }
  }
}

const saveTruck = async ({ userId, type }) => {
  const { dimensions, payload } = getDimension(type);

  TruckJoiSchema.validate({ type });
  const truck = new Truck({
    created_by: userId,
    assigned_to: 'null',
    type,
    status: 'IS',
    created_date: new Date(),
    dimensions,
    payload_max: payload,
  });

  // eslint-disable-next-line no-return-await
  return await truck.save();
};

const updateTruck = async ({ type, truckId, userId }) => {
  try {
    const { dimensions, payload } = getDimension(type);
    return await Truck.findOneAndUpdate({
      _id: truckId,
      created_by: userId,
      assigned_to: { $eq: 'null' },
    }, {
      $set: {
        type,
        dimensions,
        payload_max: payload,
      },
    }, { new: true });
  } catch (err) {
    throw new Error(err);
  }
};

// eslint-disable-next-line no-return-await
const getAllAssignedTruck = async ({ payload, dimensions }) => await Truck.findOne({
  assigned_to: { $ne: 'null' },
  status: { $eq: 'IS' },
  payload_max: { $gt: payload },
  'dimensions.width': { $gt: dimensions.width },
  'dimensions.length': { $gt: dimensions.length },
  'dimensions.height': { $gt: dimensions.height },
});

module.exports = {
  saveTruck,
  updateTruck,
  getAllAssignedTruck,
};
