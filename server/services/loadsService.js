const { Load, LoadJoiSchema } = require('../models/Loads');

const saveLoad = async ({
  // eslint-disable-next-line camelcase
  userId, name, payload, pickupAddress, deliveryAddress, dimensions, created_date,
}) => {
  LoadJoiSchema.validate({
    name, payload, pickup_address: pickupAddress, delivery_address: deliveryAddress, dimensions,
  });
  const load = new Load({
    created_by: userId,
    status: 'NEW',
    name,
    payload,
    pickup_address: pickupAddress,
    delivery_address: deliveryAddress,
    dimensions,
    // eslint-disable-next-line camelcase
    created_date: created_date || new Date(),
  });

  // eslint-disable-next-line no-return-await
  return await load.save();
};

const changeState = (state) => {
  switch (state) {
    case 'En route to Pick Up': {
      return 'Arrive to Pick Up';
    }
    case 'Arrive to Pick Up': {
      return 'En route to delivery';
    }
    case 'En route to delivery': {
      return 'Arrive to delivery';
    }
    default: {
      return 'SHIPPED';
    }
  }
};

module.exports = {
  saveLoad,
  changeState,
};
