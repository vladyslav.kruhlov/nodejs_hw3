const bcryptjs = require('bcryptjs');
const { User, UsersJoiSchema } = require('../models/Users');

const saveUser = async ({
  email,
  password,
  role,
  name,
}) => {
  UsersJoiSchema.validate({
    name, email, password, role,
  });
  const user = new User({
    name,
    email,
    password: await bcryptjs.hash(password, 10),
    role,
    createdDate: new Date(),
  });

  // eslint-disable-next-line no-return-await
  return await user.save();
};

module.exports = {
  saveUser,
};
