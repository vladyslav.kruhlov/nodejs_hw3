const path = require('path');
const express = require('express');

const localePort = 80;
const rootDir = path.join(__dirname, '../client/dist/client');
const locales = ['en-US', 'uk'];
const defaultLocale = 'en-US';
const localeServer = express();

localeServer.use(express.static(rootDir));

locales.forEach((locale) => {
  localeServer.get(`/${locale}/*`, (req, res) => {
    res.sendFile(
      path.resolve(rootDir, locale, 'index.html'),
    );
  });
});
localeServer.get('/', (req, res) => res.redirect(`/${defaultLocale}`));

const start = async () => {
  try {
    localeServer.listen(localePort, () => console.log(`App running at port ${localePort}…`));
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();
