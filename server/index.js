const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const bodyParser = require('body-parser');
const { Server } = require('socket.io');

// localization
const path = require('path');

const app = express();
const mongoose = require('mongoose');
const http = require('http').createServer(app);

const io = new Server(http, {
  cors: {
    origin: '*',
  },
});
require('./socket/index')(io);

const { authRouter } = require('./routers/authRouter');
const { usersRouter } = require('./routers/usersRouter');
const { trucksRouter } = require('./routers/trucksRouter');
const { loadsRouter } = require('./routers/loadRouter');
const { weatherRouter } = require('./routers/weatherRouter');
const { imageRouter } = require('./routers/imageRouter');
const { chatsRouter } = require('./routers/chatsRouter');

mongoose.connect('mongodb+srv://weeny:test@sandbox.xq1id.mongodb.net/?retryWrites=true&w=majority');

app.use(bodyParser({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ extended: true, limit: '50mb' }));
app.use(cors());
app.use(express.json());
app.use(morgan('dev'));
app.use(express.static(path.join(__dirname, 'server')));

app.set('view engine', 'ejs');

app.use('/api/auth', authRouter);
app.use('/api/users', usersRouter);
app.use('/api/trucks', trucksRouter);
app.use('/api/loads', loadsRouter);
app.use('/api/weather', weatherRouter);
app.use('/api/image', imageRouter);
app.use('/api/chats', chatsRouter);

// //socket.io
// eslint-disable-next-line consistent-return
io.use((socket, next) => {
  const { username } = socket.handshake.auth;
  if (!username) {
    return next(new Error('invalid username'));
  }
  // eslint-disable-next-line no-param-reassign
  socket.username = username;

  next();
});

const start = async () => {
  try {
    http.listen(8080);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();

function errorHandler(err, req, res) {
  res.status(500).send({ message: err });
}

app.use(errorHandler);
