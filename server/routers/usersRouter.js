const express = require('express');

const router = express.Router();
const { getMyProfileInfo, changeMyPassword, deleteAccount } = require('../controllers/usersController');
const { authMiddleware } = require('../middleware/authMiddleware');

router.get('/me', authMiddleware, getMyProfileInfo);

router.patch('/me/password', authMiddleware, changeMyPassword);

router.delete('/me', authMiddleware, deleteAccount);

module.exports = {
  usersRouter: router,
};
