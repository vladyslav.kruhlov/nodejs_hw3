const express = require('express');

const router = express.Router();
const { authMiddleware } = require('../middleware/authMiddleware');
const {
  getAllUsersTrucks,
  postTruck,
  getTruckById,
  deleteTruckById,
  assignTruck,
  updateTruckInfo,
} = require('../controllers/trucksController');

router.get('/', authMiddleware, getAllUsersTrucks);

router.get('/:truckId', authMiddleware, getTruckById);

router.post('/', authMiddleware, postTruck);

router.delete('/:truckId', authMiddleware, deleteTruckById);

router.post('/:truckId/assign', authMiddleware, assignTruck);

router.put('/:truckId', authMiddleware, updateTruckInfo);

module.exports = {
  trucksRouter: router,
};
