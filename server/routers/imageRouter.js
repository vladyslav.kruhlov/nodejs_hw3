const express = require('express');
const { uploadImage, upload, getImage } = require('../controllers/imageController');
const { authMiddleware } = require('../middleware/authMiddleware');

const router = express.Router();

router.get('/', authMiddleware, getImage);

router.post('/upload', authMiddleware, upload.single('image'), uploadImage);

module.exports = {
  imageRouter: router,
};
