const express = require('express');
const { getChatInfoById } = require('../controllers/chatsController');
const { authMiddleware } = require('../middleware/authMiddleware');

const router = express.Router();

router.get('/:chatID', authMiddleware, getChatInfoById);

module.exports = {
  chatsRouter: router,
};
