const express = require('express');
const { getWeather } = require('../controllers/weatherController');
const { authMiddleware } = require('../middleware/authMiddleware');

const router = express.Router();

router.put('/', authMiddleware, getWeather);

module.exports = {
  weatherRouter: router,
};
