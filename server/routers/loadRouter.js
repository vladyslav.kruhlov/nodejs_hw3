const express = require('express');

const router = express.Router();
const { authMiddleware } = require('../middleware/authMiddleware');
const {
  getAllUsersLoads,
  addLoad,
  getLoadById,
  postLoadById,
  getActiveLoad,
  changeActiveLoadState,
  deleteLoad,
  getFullLoadInfo,
  uploadLoad,
} = require('../controllers/loadsController');

router.get('/', authMiddleware, getAllUsersLoads);

router.get('/active', authMiddleware, getActiveLoad);

router.get('/:loadId', authMiddleware, getLoadById);

router.get('/:loadId/shipping_info', authMiddleware, getFullLoadInfo);

router.put('/:loadId', authMiddleware, uploadLoad);

router.post('/', authMiddleware, addLoad);

router.post('/:loadId/post', authMiddleware, postLoadById);

router.patch('/active/state', authMiddleware, changeActiveLoadState);

router.delete('/:loadId', authMiddleware, deleteLoad);

module.exports = {
  loadsRouter: router,
};
