const { Chat } = require('../models/Chats');

module.exports = (io) => {
  io.on('connection', (socket) => {
    console.log(`user connected ${socket.username}`);

    socket.on('chat message', async ({
      content, chatID, from, userID,
    }) => {
      console.log(`message: ${content} chat: ${chatID} from: ${from}`);
      await Chat.findOneAndUpdate({
        _id: chatID,
      }, {
        $push: {
          messages: {
            from,
            content,
            userID,
          },
        },
      });
    });

    socket.on('disconnect', () => console.log(`user ${socket.username} disconnected`));
  });
};
