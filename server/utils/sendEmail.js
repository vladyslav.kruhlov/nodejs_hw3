const mailgun = require('mailgun-js');

const DOMAIN = 'sandboxd30cd345e02b4e58aa077d7d26a7a96c.mailgun.org';
const mg = mailgun({ apiKey: '9f70d7f1568aeb43187b0f4920b8cea0-4dd50799-d373bd3a', domain: DOMAIN });

const sendEmail = async ({ text, email, subject }) => {
  const message = {
    from: 'Excited User <me@samples.mailgun.org>', // sender address
    to: email,
    subject,
    text,
    template: 'forget_password',
  };

  await mg.messages().send(message, (error, body) => {
    console.log(body, error);
  });
};

module.exports = {
  sendEmail,
};
