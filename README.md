## RUN SERVER PART

#### Enter all commant in console(IMPORTANT!!!)
-`cd server`

-`npm i` or `npm install` to install all necessary dependencies

-`npm run start` to run server or `npm run watch` to run watch mode (where you can detect and without restarting the server your changes)

after all commands you can see your server here: `http://localhost:8080`




------------

## RUN PRODUCTION SERVER WITH L10N

-`cd server`

-if you already did `npm install` you can skip this step

-then move to `client` folder and run this command `ng build --localize`

-`npm run localization` and also for normal work you should run server part(for this look previous topic RUN SERVER PART)

-after all steps you can use app by this link `http://localhost:80`

------------



## RUN CLIENT(development mode) PART

-`cd client`

-`npm i` or `npm install` to install all necessary dependencies

-`ng serve` or `npm run start` will run on development mode and also you can specify configuration and run `en-US` version or `uk` by this command `ng serve --configuration en-US` or `ng serve --configuration uk`

if you want to run production mode you can also just run server and in your browser open `http://localhost:80` and there you will see production build with all features include **localization**
