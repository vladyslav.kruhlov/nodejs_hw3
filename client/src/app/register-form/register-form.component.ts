import { Observable, Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';
import { getLoadingStatus } from '../reducers';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { AuthAPIService } from '../services/AuthAPI.service';
import { NotificationService } from '../services/notification.service';
import { Loading } from '../reducers/main/main.reducer';
import { LoadingOff, LoadingOn } from '../reducers/main/main.actions';

@Component({
  selector: 'app-register',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.scss']
})
export class RegisterFormComponent implements OnInit {
  registerForm = new FormGroup( {
    name: new FormControl<string>(''),
    email: new FormControl<string>( '' ),
    confirm: new FormControl<string>( '' ),
    password: new FormControl<string>( '' ),
    role: new FormControl<string>('' ),
  } )
  load$!: Observable<boolean>;
  isLoading = false;

  constructor(
    private notification: NotificationService,
    private router: Router,
    private store: Store<Loading>,
    private AuthAPI: AuthAPIService,
  ) {
    this.load$ = store.select(getLoadingStatus);
    this.load$.subscribe((status) => {
      this.isLoading = status;
    })
  }

  ngOnInit(): void {
    if(localStorage.getItem('jwt-token')) {
      this.router.navigate(['']);
    }
  }

  onSubmit(): Subscription|void {
    const { email, role, confirm, password, name } = this.registerForm.value;

    if (confirm !== password) {
      this.notification.openSnackBar('Your passwords don\'t match');
      setTimeout(() => this.notification.closeSnackBar(), 4000);
      return;
    }
    this.store.dispatch(LoadingOn());
    return this.AuthAPI.register(email!, role!, password!, name!).subscribe({
      next: response => {
        if(response.status === 200) {
          this.notification.openSnackBar('User successfully registered. You can login')
          setTimeout(() => this.notification.closeSnackBar(), 4000)
          this.router.navigate(['/login'])
          this.store.dispatch(LoadingOff());
        }
      },
      error: error => {
        this.store.dispatch(LoadingOff());
        this.notification.openSnackBar(error.error.message || 'Something went wrong')
        setTimeout(() => this.notification.closeSnackBar(), 4000);
      }
    })
  }
}
