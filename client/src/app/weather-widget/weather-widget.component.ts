import { take } from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import { GeolocationService } from '@ng-web-apis/geolocation';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NotificationService } from '../services/notification.service';

@Component({
  selector: 'app-weather-widget',
  templateUrl: './weather-widget.component.html',
  styleUrls: ['./weather-widget.component.scss']
})
export class WeatherWidgetComponent implements OnInit {
  WeatherData:any;

  constructor(
    private http: HttpClient,
    private readonly geolocation$: GeolocationService,
    private notification: NotificationService,
  ) { }

  ngOnInit() {
    this.WeatherData = {
      main : {},
      isDay: true
    };
    this.getWeatherData();
  }

  async getWeatherData(){

    this.geolocation$.pipe(take(1)).subscribe(( position: any) => {
      this.http.put<any>('http://localhost:8080/api/weather', {
        lon: position.coords.longitude,
        lat: position.coords.latitude
      }, {
        headers: new HttpHeaders( {
          'Authorization': 'Bearer ' + localStorage.getItem( 'jwt-token' )
        } )
      }).subscribe({
        next: (res) => {
          if (res.status === 200) {
            this.setWeatherData(res.data);
          }
        },
        error: () => {
          this.notification.openSnackBar('We cannot get weather info');
          setTimeout(() => this.notification.closeSnackBar(), 4000);
        }
      })
    })
  }

  setWeatherData(data: any){
    this.WeatherData = data;
    let sunsetTime = new Date(this.WeatherData.sys.sunset * 1000);
    this.WeatherData.sunset_time = sunsetTime.toLocaleTimeString();
    let currentDate = new Date();
    this.WeatherData.isDay = (currentDate.getTime() < sunsetTime.getTime());
    this.WeatherData.temp_celcius = (this.WeatherData.main.temp - 273.15).toFixed(0);
    this.WeatherData.temp_min = (this.WeatherData.main.temp_min - 273.15).toFixed(0);
    this.WeatherData.temp_max = (this.WeatherData.main.temp_max - 273.15).toFixed(0);
    this.WeatherData.temp_feels_like = (this.WeatherData.main.feels_like - 273.15).toFixed(0);
  }

}
