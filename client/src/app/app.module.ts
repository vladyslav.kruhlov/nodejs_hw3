import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EmailFormComponent } from './email-form/email-form.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatIconModule } from '@angular/material/icon';
import { MatDialogModule } from '@angular/material/dialog';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { HttpClientJsonpModule, HttpClientModule } from '@angular/common/http';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MainPageComponent } from './main-page/main-page.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { RegisterFormComponent } from './register-form/register-form.component';
import { MatSelectModule } from '@angular/material/select';
import { SpinnerComponent } from './spinner/spinner.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { LoginModalComponent } from './login-form/login-modal/login-modal.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { NewLoadsComponent } from './new-loads/new-loads.component';
import { AssignedLoadsComponent } from './assigned-loads/assigned-loads.component';
import { ChatsComponent } from './chats/chats.component';
import { ProfileComponent } from './profile/profile.component';
import { PasswordModalComponent } from './profile/password-modal/password-modal.component';
import { DeleteModalComponent } from './profile/delete-modal/delete-modal.component';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { LoadsModalComponent } from './new-loads/loads-modal/loads-modal.component';
import { ItemModalComponent } from './new-loads/item-modal/item-modal.component';
import { WeatherWidgetComponent } from './weather-widget/weather-widget.component';
import { GoogleMapsModule } from '@angular/google-maps';
import { MapModalComponent } from './new-loads/map-modal/map-modal.component';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { reducers, metaReducers } from './reducers';
import { ErrorPageComponent } from './error-page/error-page.component';
import { TruckComponent } from './truck/truck.component';
import { TruckInfoModalComponent } from './truck/truck-info-modal/truck-info-modal.component';
import { TruckAssignModalComponent } from './truck/truck-assign-modal/truck-assign-modal.component';
import { TruckDeleteModalComponent } from './truck/truck-delete-modal/truck-delete-modal.component';
import { MatTabsModule } from '@angular/material/tabs';
import { ActiveLoadsComponent } from './new-loads/active-loads/active-loads.component';
import { MessageComponent } from './chats/message/message.component';
import { LocaleSwitcherComponent } from './locale-switcher/locale-switcher.component';
import { SocketIoConfig, SocketIoModule } from 'ngx-socket-io';
import { environment } from '../environments/environment';
import { MatTooltipModule } from '@angular/material/tooltip';

const config: SocketIoConfig = {
  url: environment.socketUrl, // socket server url;
  options: {
    transports: ['websocket']
  }
}

@NgModule( {
  declarations: [
    AppComponent,
    EmailFormComponent,
    MainPageComponent,
    LoginFormComponent,
    RegisterFormComponent,
    SpinnerComponent,
    LoginModalComponent,
    NewLoadsComponent,
    AssignedLoadsComponent,
    ChatsComponent,
    ProfileComponent,
    PasswordModalComponent,
    DeleteModalComponent,
    LoadsModalComponent,
    ItemModalComponent,
    WeatherWidgetComponent,
    MapModalComponent,
    ErrorPageComponent,
    TruckComponent,
    TruckInfoModalComponent,
    TruckAssignModalComponent,
    TruckDeleteModalComponent,
    ActiveLoadsComponent,
    MessageComponent,
    LocaleSwitcherComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatButtonModule,
    MatCardModule,
    MatIconModule,
    MatDialogModule,
    MatDividerModule,
    MatMenuModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatSnackBarModule,
    FormsModule,
    MatCheckboxModule,
    MatSelectModule,
    MatProgressSpinnerModule,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    GoogleMapsModule,
    CommonModule,
    HttpClientJsonpModule,
    StoreModule.forRoot( reducers, {
      metaReducers
    } ),
    SocketIoModule.forRoot( config ),
    MatTabsModule,
    MatTooltipModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
} )
export class AppModule {
}
