import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { Component, Inject, OnInit } from '@angular/core';
import { getLoadingStatus } from '../../reducers';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { TruckAPIService } from '../../services/TruckAPI.service';
import { NotificationService } from '../../services/notification.service';
import { Truck } from '../../../interfaces/trucks';
import { Loading } from '../../reducers/main/main.reducer';
import { LoadingOff, LoadingOn } from '../../reducers/main/main.actions';


@Component( {
  selector: 'app-truck-info-modal',
  templateUrl: './truck-info-modal.component.html',
  styleUrls: ['./truck-info-modal.component.scss']
} )
export class TruckInfoModalComponent implements OnInit {
  selected: string | undefined;
  load$!: Observable<boolean>;
  isLoading: boolean = false;

  constructor(
    private notification: NotificationService,
    private dialogRef: MatDialogRef<TruckInfoModalComponent>,
    @Inject(MAT_DIALOG_DATA) public truck: Truck,
    private store: Store<Loading>,
    private TruckAPI: TruckAPIService,
  ) {
    this.load$ = store.select(getLoadingStatus);
    this.load$.subscribe((status) => {
      this.isLoading = status;
    })
  }

  ngOnInit(): void {
    if(this.truck) {
      this.selected = this.truck.type;
    }
  }

  addTruck(): void {
    this.store.dispatch(LoadingOn());
    this.TruckAPI.addTruck(this.selected!).subscribe( {
      next: ( res ) => {
        if (res.status === 200) {
          this.dialogRef.close(res.truck);
          this.notification.openSnackBar(res.message)
          setTimeout(() => this.notification.closeSnackBar(), 4000);
          this.store.dispatch(LoadingOff());
        }
      },
      error: () => {
        this.notification.openSnackBar('We cannot post your truck')
        setTimeout(() => this.notification.closeSnackBar(), 4000);
        this.store.dispatch(LoadingOff());
      }
    } )
  }

  updateTruck(): void {
    if(this.selected === this.truck.type) {
      this.notification.openSnackBar(`Your truck already ${this.truck.type} type`)
      setTimeout(() => this.notification.closeSnackBar(), 4000);
      return;
    }
    this.store.dispatch(LoadingOn());

    this.TruckAPI.updateTruckInfo(this.truck._id, this.selected!).subscribe({
      next: (res) => {
        if (res.status === 200) {
          this.notification.openSnackBar(res.message)
          setTimeout(() => this.notification.closeSnackBar(), 4000);
          this.store.dispatch(LoadingOff());
          this.dialogRef.close(res.truck)
        }
      },
      error: (err) => {
        this.notification.openSnackBar(err.error.message)
        setTimeout(() => this.notification.closeSnackBar(), 4000);
        this.store.dispatch(LoadingOff());
        this.dialogRef.close();
      }
    })
  }
}
