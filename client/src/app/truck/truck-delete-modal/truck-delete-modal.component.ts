import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Truck } from '../../../interfaces/trucks';

@Component({
  selector: 'app-truck-delete-modal',
  templateUrl: './truck-delete-modal.component.html',
  styleUrls: ['./truck-delete-modal.component.scss']
})
export class TruckDeleteModalComponent {
  constructor(
    @Inject(MAT_DIALOG_DATA) public truck: Truck,
    private dialogRef: MatDialogRef<TruckDeleteModalComponent>,
  ) { }

  deleteTruck(): void {
    this.dialogRef.close(true);
  }

  closeDialog(): void {
    this.dialogRef.close(false);
  }
}
