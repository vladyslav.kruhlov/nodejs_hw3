import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Truck } from '../../../interfaces/trucks';

@Component({
  selector: 'app-truck-assign-modal',
  templateUrl: './truck-assign-modal.component.html',
  styleUrls: ['./truck-assign-modal.component.scss']
})
export class TruckAssignModalComponent {
  constructor(
    @Inject(MAT_DIALOG_DATA) public truck: Truck,
    private dialogRef: MatDialogRef<TruckAssignModalComponent>,
  ) { }

  assignTruck(): void {
    this.dialogRef.close(true);
  }

  closeDialog(): void {
    this.dialogRef.close(false);
  }
}
