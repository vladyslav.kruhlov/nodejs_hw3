import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { getLoadingStatus, getUserId } from '../reducers';
import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LiveAnnouncer } from '@angular/cdk/a11y';
import { MatSort, Sort } from '@angular/material/sort';
import { DialogService } from '../services/dialog.service';
import { MatTableDataSource } from '@angular/material/table';
import { TruckAPIService } from '../services/TruckAPI.service';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { NotificationService } from '../services/notification.service';
import { TruckInfoModalComponent } from './truck-info-modal/truck-info-modal.component';
import { TruckDeleteModalComponent } from './truck-delete-modal/truck-delete-modal.component';
import { TruckAssignModalComponent } from './truck-assign-modal/truck-assign-modal.component';
import { Truck } from '../../interfaces/trucks';
import { LoadingOff, LoadingOn } from '../reducers/main/main.actions';
import { ProfileInterface } from '../reducers/profile/profile.reducer';

@Component({
  selector: 'app-truck',
  templateUrl: './truck.component.html',
  styleUrls: ['./truck.component.scss']
})
export class TruckComponent implements OnInit {
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  loads!: MatTableDataSource<Truck>;
  displayedColumns: string[] = [
    'index', 'id', 'type', 'status', 'assigned_to', 'date', 'payload', 'dimensions', 'edit', 'assign', 'delete'
  ]
  length!: number;
  pageSize: number = 10;
  pageSizeOptions: number[] = [ 10, 20, 30, 40, 50];
  pageIndex = 1;
  trucks: Truck[] = [];
  load$!: Observable<boolean>;
  isLoading: boolean = false;
  id$!: Observable<string>;
  userID!: string;

  constructor(
    private dialog: DialogService,
    private http: HttpClient,
    private notification: NotificationService,
    private _liveAnnouncer: LiveAnnouncer,
    private store: Store<ProfileInterface>,
    private TruckAPI: TruckAPIService
  ) {
    this.id$ = this.store.select(getUserId);
    this.id$.subscribe((id) => {
      this.userID = id;
    })
    this.load$ = store.select(getLoadingStatus);
    this.load$.subscribe((status) => {
      this.isLoading = status;
    })
    store.dispatch(LoadingOn());
  }

  ngOnInit(): void {
    this.TruckAPI.getTruck().subscribe({
      next: (res) => {
        if (res.status === 200) {
          this.trucks = res.trucks.reverse()
          this.setTable();
          this.store.dispatch(LoadingOff());
        }
      },
      error: () => {
        this.store.dispatch(LoadingOff());
      }
    })
  }

  openDialog(): void {
    const dialogRef = this.dialog.openDialog( TruckInfoModalComponent, '400px');

    dialogRef.afterClosed().subscribe((truck) => {
      if (truck) {
        this.trucks.unshift(truck);
        this.setTable();
        this.store.dispatch(LoadingOff());
      }
    })
  }

  handlePageEvent(event: PageEvent) {
    this.length = event.length;
    this.pageSize = event.pageSize;
    this.pageIndex = event.pageIndex;
  }

  announceSortChange(sortState: Sort) {
    if (sortState.direction) {
      this._liveAnnouncer.announce(`Sorted ${sortState.direction}ending`);
    } else {
      this._liveAnnouncer.announce('Sorting cleared');
    }
  }

  assignTruck(event: MouseEvent, truck: Truck) {
    const assignRef = this.dialog.openDialogWithData(TruckAssignModalComponent, truck, '580px');

    assignRef.afterClosed().subscribe((assign) => {
      if (assign) {
        this.store.dispatch(LoadingOn());
        this.TruckAPI.assignTruck(truck._id).subscribe({
          next: (res) => {
            if (res.status == 200) {
              truck.assigned_to = this.userID;
              this.notification.openSnackBar('Truck assigned successfully');
              setTimeout(() => this.notification.closeSnackBar(), 4000);
              this.store.dispatch(LoadingOff());
            }
          },
          error: (err) => {
            this.notification.openSnackBar(err.error.message);
            setTimeout(() => this.notification.closeSnackBar(), 4000);
            this.store.dispatch(LoadingOff());
          }
        })
      }
    })
  }

  deleteTruck(event: MouseEvent, truck: Truck) {
    event.stopPropagation()

    const deleteRef = this.dialog.openDialogWithData(TruckDeleteModalComponent, truck, '580px')

    deleteRef.afterClosed().subscribe((result) => {
      if(result) {
        this.store.dispatch(LoadingOn());
        this.TruckAPI.deleteTruck(truck._id).subscribe({
          next: (res) => {
            if (res.status === 200) {
              const index = this.trucks.indexOf(truck);
              this.trucks.splice(index, 1);
              this.setTable()
              this.store.dispatch(LoadingOff());
              this.notification.openSnackBar('Truck deleted successfully');
              setTimeout(() => this.notification.closeSnackBar(), 4000);

            }
          },
          error: (err) => {
            this.notification.openSnackBar(err.error.message);
            setTimeout(() => this.notification.closeSnackBar(), 4000);
            this.store.dispatch(LoadingOff());
          }
        })
      }
    })
  }

  updateTruck( event: MouseEvent, truck: Truck ) {
    event.stopPropagation()

    const editRef = this.dialog.openDialogWithData(TruckInfoModalComponent, truck, '400px')

    editRef.afterClosed().subscribe((updatedTruck) => {
      if (updatedTruck) {
        const index = this.trucks.indexOf(truck);
        this.trucks[index] = updatedTruck;
        this.setTable();
      }
    })
  }

  setTable() {
    this.length = this.trucks.length;
    this.loads = new MatTableDataSource<Truck>(this.trucks);
    this.loads.paginator = this.paginator;
    this.loads.sort = this.sort;
  }
}
