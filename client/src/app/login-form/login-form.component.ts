import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';
import { getLoadingStatus } from '../reducers';
import { FormControl, FormGroup } from '@angular/forms';
import { Component, Injectable, OnInit } from '@angular/core';
import { DialogService } from '../services/dialog.service';
import { AuthAPIService } from '../services/AuthAPI.service';
import { NotificationService } from '../services/notification.service';
import { LoginModalComponent } from './login-modal/login-modal.component';
import { Loading } from '../reducers/main/main.reducer';
import { LoadingOff, LoadingOn } from '../reducers/main/main.actions';


@Component( {
  selector: 'app-login',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
} )

@Injectable()
export class LoginFormComponent implements OnInit {
  loginForm = new FormGroup( {
    email: new FormControl(),
    password: new FormControl(),
  } )
  load$!: Observable<boolean>;
  isLoading!: boolean;

  constructor(
    private AuthAPI: AuthAPIService,
    private notification: NotificationService,
    private router: Router,
    private dialog: DialogService,
    private store: Store<Loading>,
  ) {
    this.load$ = store.select( getLoadingStatus );
    this.load$.subscribe( ( status ) => {
      this.isLoading = status;
    } )
  }

  ngOnInit(): void {
    if (localStorage.getItem( 'jwt-token' )) {
      this.router.navigate( [''] );
    }
  }

  openDialog(): void {
    const dialogRef = this.dialog.openDialog( LoginModalComponent, '400px' );

    dialogRef.afterClosed().subscribe( ( result ) => {
      if (result) {
        this.AuthAPI.forgotPassword( result ).subscribe( {
          next: ( res ) => {
            if (res.status === 200) {
              this.notification.openSnackBar( 'We send you reset password email' )
              setTimeout( () => this.notification.closeSnackBar(), 4000 )
            }
          },
          error: ( err ) => {
            this.notification.openSnackBar( err.error.message );
            setTimeout( () => this.notification.closeSnackBar(), 4000 )
          }
        } )
      }
    } )
  }

  onSubmit(): void {
    this.store.dispatch( LoadingOn() );
    this.AuthAPI.login( this.loginForm.value.email, this.loginForm.value.password ).subscribe( {
      next: ( res ) => {
        if (res.status === 200) {
          localStorage.setItem( 'jwt-token', res.jwt_token );
          this.store.dispatch( LoadingOff() );
          this.notification.openSnackBar( 'User successfully sign in' )
          setTimeout( () => this.notification.closeSnackBar(), 4000 )
          this.router.navigate( [''] )
        }
      },
      error: () => {
        this.loginForm.patchValue( { password: '' } );
        this.notification.openSnackBar( 'Email or Password is incorrect. Try again' )
        setTimeout( () => this.notification.closeSnackBar(), 4000 )
        this.store.dispatch( LoadingOff() );
      }
    } )
  }
}
