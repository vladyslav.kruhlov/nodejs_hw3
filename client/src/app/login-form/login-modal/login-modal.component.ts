import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-login-modal',
  templateUrl: './login-modal.component.html',
  styleUrls: ['./login-modal.component.scss']
})
export class LoginModalComponent {
  modalForm = new FormGroup({
    email: new FormControl(''),
  })

  constructor(private dialogRef: MatDialogRef<LoginModalComponent>) { }

  onSubmit(): void {
    this.dialogRef.close(this.modalForm.value.email);
  }
}
