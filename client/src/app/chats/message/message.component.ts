import { Component, ElementRef, OnInit, Renderer2, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { FormControl, FormGroup } from '@angular/forms';
import { SocketService } from '../../services/socket.service';
import { Chat, ProfileInterface } from '../../reducers/profile/profile.reducer';
import { Store } from '@ngrx/store';
import { getLoadingStatus, getUserChats, getUserId, getUserName } from '../../reducers';
import { DataService } from '../../services/data.service';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MessageResponse } from '../../../interfaces';
import { LoadingOff, LoadingOn } from '../../reducers/main/main.actions';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss'],
  providers: [DataService],
})
export class MessageComponent implements OnInit {
  @ViewChild('chatWindow') chatWindow!: ElementRef;

  name$!: Observable<string>;
  userName!: string;
  id$!: Observable<string>;
  userID!: string;
  chats$!: Observable<Chat[]>;
  userChats!: Chat[];
  chatID!: string;
  messages: { from: string; content: string; }[] = [];
  load$!: Observable<boolean>;
  isLoading: boolean  = false;
  data!: Chat | undefined;

  messageForm = new FormGroup({
    message: new FormControl(''),
  })

  constructor(
    private store: Store<ProfileInterface>,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private socket: SocketService,
    private http: HttpClient,
    private rendered: Renderer2,
  ) {
    router.routeReuseStrategy.shouldReuseRoute = () => false;

    this.name$ = store.select(getUserName);
    this.name$.subscribe((name) => {
      this.userName = name;
    })
    this.id$ = store.select(getUserId);
    this.id$.subscribe((id) => {
      this.userID = id;
    })
    this.chats$ = store.select(getUserChats);
    this.chats$.subscribe((chats) => {
      this.userChats = chats;
    })
    this.load$ = store.select(getLoadingStatus);
    this.load$.subscribe((status) => {
      this.isLoading = status;
    })
    store.dispatch(LoadingOn());
  }

  ngOnInit() {
    setTimeout(() => {
      let validation = false;
      this.userChats.forEach(({ id }) => {
        if(id === this.router.url.replace('/chats/', '')) {
          validation = true
        }
      })
      if (!validation && this.router.url !== '/chats') {
        this.store.dispatch(LoadingOff());
        this.router.navigate(['/404'])
      }
      this.chatID = this.router.url.replace('/chats/', '');
      this.http.get<MessageResponse>(`http://localhost:8080/api/chats/${this.chatID}`, {
        headers: new HttpHeaders( {
          'Authorization': 'Bearer ' + localStorage.getItem( 'jwt-token' )
        } )
      }).subscribe({
        next: (res) => {
          if (res.status === 200) {
            res.messages.forEach((message) => {
              this.showMessagesOnFirstLoad(message);
              this.store.dispatch(LoadingOff());
            })
          }
        },
        error: () => {
          this.store.dispatch(LoadingOff());
        }
      })
    }, 1000);
  }

  onMessage() {
    if (this.messageForm.value.message) {
      this.socket.sendMessage({
        content: this.messageForm.value.message,
        chatID: this.chatID,
        from: this.userName,
        userID: this.userID,
      });

      this.showMessagesOnFirstLoad({
        from: this.userName,
        content: this.messageForm.value.message,
        userID: this.userID,
      });
    }

    this.messageForm.reset();
  }

  showMessagesOnFirstLoad({from, content, userID}: { from: string; content: string; userID: string;}) {
    const li = this.rendered.createElement('li');
    this.rendered.addClass(li, 'messages__message');
    if(userID === this.userID) {
      this.rendered.addClass(li, 'messages__message-me');
    } else {
      this.rendered.addClass(li, 'messages__message-friend');
    }
    const name = document.createElement('p');
    name.textContent = from;
    const text = document.createElement('p');
    text.textContent = content
    li.append(name);
    li.append(content)
    this.chatWindow.nativeElement.prepend(li);
  }
}
