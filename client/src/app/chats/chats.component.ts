import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { getUserChats, getUserName } from '../reducers';
import { Component, OnDestroy, OnInit, } from '@angular/core';
import { DataService } from '../services/data.service';
import { SocketService } from '../services/socket.service';
import { Chat, ProfileInterface } from '../reducers/profile/profile.reducer';

@Component({
  selector: 'app-chats',
  templateUrl: './chats.component.html',
  styleUrls: ['./chats.component.scss'],
  providers: [DataService]
})
export class ChatsComponent implements OnInit, OnDestroy {
  chats$!: Observable<Chat[]>;
  userChats!: Chat[];
  name$!: Observable<string>;
  userName!: string;

  constructor(
    private store: Store<ProfileInterface>,
    private socket: SocketService,
    private dataService: DataService,
  ) {
    this.chats$ = store.select(getUserChats);
    this.chats$.subscribe((chats) => {
      this.userChats = chats;
    })
  }

  ngOnInit(): void {
    this.name$ = this.store.select(getUserName);
    this.name$.subscribe((name) => {
      this.userName = name;
      this.socket.connect(this.userName)
    })
  }

  ngOnDestroy() {
    this.socket.disconnect()
  }

  connectToRoom(user: any) {
    this.dataService.sendData(user);
  }
}
