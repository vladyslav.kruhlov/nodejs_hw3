import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EmailFormComponent } from './email-form/email-form.component';
import { MainPageComponent } from './main-page/main-page.component';
import { RegisterFormComponent } from './register-form/register-form.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { NewLoadsComponent } from './new-loads/new-loads.component';
import { AssignedLoadsComponent } from './assigned-loads/assigned-loads.component';
import { ChatsComponent } from './chats/chats.component';
import { ProfileComponent } from './profile/profile.component';
import { ErrorPageComponent } from './error-page/error-page.component';
import { TruckComponent } from './truck/truck.component';
import { MessageComponent } from './chats/message/message.component';
import { NewsResolver } from './services/Resolver';

const routes: Routes = [
  { path: '', redirectTo: '/myLoads', pathMatch: 'full' },
  { path: '', component: MainPageComponent, children: [
      { path: 'myLoads', component: NewLoadsComponent,  },
      { path: 'assignedLoads', component: AssignedLoadsComponent,  },
      { path: 'profile', component: ProfileComponent,  },
      { path: 'trucks', component:  TruckComponent,},
      { path: 'chats', component: ChatsComponent, children: [
          { path: ':id', component: MessageComponent, resolve: {data: NewsResolver} }
        ] },
    ] },
  { path: '404', component: ErrorPageComponent },
  { path: 'signUp', component: RegisterFormComponent },
  { path: 'login', component: LoginFormComponent },
  { path: 'resetPassword', component: EmailFormComponent },
  { path: '**', redirectTo: '/404' },

];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: false })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
