import jsPDF from 'jspdf';
import { Observable } from 'rxjs';
import html2canvas from 'html2canvas';
import * as FileSaver from 'file-saver';
import { Store } from '@ngrx/store';
import { Component, ElementRef, Inject, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { getLoadingStatus, getUserRole } from '../../reducers';
import { MapModalComponent } from '../map-modal/map-modal.component';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { DialogService } from '../../services/dialog.service';
import { LoadAPIService } from '../../services/LoadAPI.service';
import { NotificationService } from '../../services/notification.service';
import { Load } from '../../../interfaces/loads';
import { LoadingOff, LoadingOn } from '../../reducers/main/main.actions';
import { ProfileInterface } from '../../reducers/profile/profile.reducer';

@Component({
  selector: 'app-item-modal',
  templateUrl: './item-modal.component.html',
  styleUrls: ['./item-modal.component.scss']
})
export class ItemModalComponent {
  @ViewChild('invoice') invoiceElement!: ElementRef;
  @ViewChild('buttons') buttons!: ElementRef;
  @ViewChild('map') mapBtn!: ElementRef;

  role$!: Observable<string>;
  userRole!: string;
  load$!: Observable<boolean>;
  isLoading: boolean = false;

  constructor(
    private dialogRef: MatDialogRef<ItemModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Load,
    public dialog: DialogService,
    private store: Store<ProfileInterface>,
    private http: HttpClient,
    private notification: NotificationService,
    private LoadAPI: LoadAPIService,
    ) {
    this.role$ = store.select(getUserRole)
    this.role$.subscribe((role) => {
      this.userRole = role;
    })
    this.load$ = store.select(getLoadingStatus);
    this.load$.subscribe((status) => {
      this.isLoading = status;
    })
  }

  postLoad(): void {
    this.dialogRef.close('POST')
  }

  deleteLoad(): void {
    this.dialogRef.close('DELETE');
  }

  openMap(): void {
    this.dialogRef.close();
    this.dialog.openDialogWithData( MapModalComponent, {
      delivery: this.data.delivery_address,
      pickUp: this.data.pickup_address,
    }, '600px');
  }

  iterateLoad(): void {
    if (this.data.status !== 'SHIPPED') {
      this.store.dispatch(LoadingOn());
      this.LoadAPI.iterateLoadStatus().subscribe({
        next: (res) => {
          if (res.status === 200) {
            if (res.state === 'Arrive to delivery') {
              this.data.status = 'SHIPPED';
            }
            this.data.state = res.state;
            this.notification.openSnackBar(res.message)
            setTimeout(() => this.notification.closeSnackBar(), 4000);
            this.store.dispatch(LoadingOff());
          }
        },
        error: (err) => {
          this.notification.openSnackBar(err.error.message)
          setTimeout(() => this.notification.closeSnackBar(), 4000);
          this.store.dispatch(LoadingOff());
        }
      })
    }
  }

  generatePDF(): void {
    this.buttons.nativeElement.style = 'display: none;'
    this.mapBtn.nativeElement.style = 'display: none;'

    html2canvas(this.invoiceElement.nativeElement, { scale: 3 }).then((canvas) => {
      const imageGeneratedFromTemplate = canvas.toDataURL('image/png');
      const fileWidth = 200;
      const generatedImageHeight = (canvas.height * fileWidth) / canvas.width;
      let PDF = new jsPDF('p', 'mm', 'a4',);
      PDF.addImage(imageGeneratedFromTemplate, 'PNG', 0, 5, fileWidth, generatedImageHeight,);
      PDF.html(this.invoiceElement.nativeElement.innerHTML)
      PDF.save('angular-invoice-pdf-demo.pdf');
    });

    this.buttons.nativeElement.style = 'display: flex;'
    this.mapBtn.nativeElement.style = 'display: flex;'
  }

  generateExcel() {
    if (this.data) {
      import("xlsx").then(xlsx => {
        const worksheet = xlsx.utils.json_to_sheet([this.data]);
        const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
        const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
        this.saveAsExcelFile(excelBuffer, "ExportExcel");
      });
    }
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
    let EXCEL_EXTENSION = '.xlsx';
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE
    });
    FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
  }
}
