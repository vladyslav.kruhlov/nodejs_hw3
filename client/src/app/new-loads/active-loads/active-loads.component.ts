import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { Component, OnInit, ViewChild } from '@angular/core';
import { getUserRole } from '../../reducers';
import { HttpClient } from '@angular/common/http';
import { LiveAnnouncer } from '@angular/cdk/a11y';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { ItemModalComponent } from '../item-modal/item-modal.component';
import { TableElementInterface } from '../../../interfaces';
import { DialogService } from '../../services/dialog.service';
import { LoadAPIService } from '../../services/LoadAPI.service';
import { NotificationService } from '../../services/notification.service';
import { Load } from '../../../interfaces/loads';
import { ProfileInterface } from '../../reducers/profile/profile.reducer';

@Component({
  selector: 'app-active-loads',
  templateUrl: './active-loads.component.html',
  styleUrls: ['./active-loads.component.scss']
})
export class ActiveLoadsComponent implements OnInit {
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  activeLoads!: MatTableDataSource<TableElementInterface>;
  dataActive: Load[] = [];
  displayedColumns: string[] = ['index', 'name', 'date', 'status', 'pickup', 'delivery', 'state', 'edit']
  length!: number;
  pageSize: number = 10;
  pageSizeOptions: number[] = [ 10, 20, 30, 40, 50];
  pageIndex = 1;
  isLoading: boolean = false;
  role$!: Observable<string>;
  userRole!: string;

  constructor(
    private http: HttpClient,
    private notification: NotificationService,
    public dialog: DialogService,
    private _liveAnnouncer: LiveAnnouncer,
    private store: Store<ProfileInterface>,
    private LoadAPI: LoadAPIService,
  ) {
    this.role$ = this.store.select(getUserRole);
    this.role$.subscribe((role) => {
      this.userRole = role;
    })
  }

  ngOnInit(): void {
    if (this.userRole === 'DRIVER') {
      this.LoadAPI.getActiveLoad().subscribe({
        next: (res) => {
          if (res.status === 200) {
            this.length = 1;
            this.dataActive = res.load ? [res.load] : [];
            this.activeLoads = new MatTableDataSource<TableElementInterface>(this.dataActive);
            this.activeLoads.paginator = this.paginator;
            this.activeLoads.sort = this.sort;
          }
        },
        error: (err) => {
          this.notification.openSnackBar(err.error.message);
          setTimeout(() => this.notification.closeSnackBar(), 4000);
        }
      })
    }
  }

  handlePageEvent(event: PageEvent) {
    this.length = event.length;
    this.pageSize = event.pageSize;
    this.pageIndex = event.pageIndex;
  }

  announceSortChange(sortState: Sort) {
    if (sortState.direction) {
      this._liveAnnouncer.announce(`Sorted ${sortState.direction}ending`);
    } else {
      this._liveAnnouncer.announce('Sorting cleared');
    }
  }

  openLoad(load: Load): void {
    this.dialog.openDialogWithData( ItemModalComponent, load, '600px');
  }
}
