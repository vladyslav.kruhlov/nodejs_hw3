import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { getLoadingStatus, getUserRole } from '../reducers';
import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LiveAnnouncer } from '@angular/cdk/a11y';
import { MatSort, Sort } from '@angular/material/sort';
import { TableElementInterface } from '../../interfaces';
import { DialogService } from '../services/dialog.service';
import { LoadAPIService } from '../services/LoadAPI.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { ItemModalComponent } from './item-modal/item-modal.component';
import { LoadsModalComponent } from './loads-modal/loads-modal.component';
import { Load } from '../../interfaces/loads';
import { Loading } from '../reducers/main/main.reducer';
import { LoadingOff, LoadingOn } from '../reducers/main/main.actions';
import { NotificationService } from '../services/notification.service';
import { ProfileInterface } from '../reducers/profile/profile.reducer';

@Component({
  selector: 'app-new-loads',
  templateUrl: './new-loads.component.html',
  styleUrls: ['./new-loads.component.scss']
})
export class NewLoadsComponent implements OnInit {
  loads!: MatTableDataSource<TableElementInterface>;
  displayedColumns: string[] = ['index', 'name', 'date', 'status', 'pickup', 'delivery', 'state', 'edit']
  length!: number;
  pageSize: number = 10;
  pageSizeOptions: number[] = [ 10, 20, 30, 40, 50];
  pageIndex: number = 1;
  data: Load[] = []
  load$!: Observable<boolean>;
  isLoading: boolean = false;
  role$!: Observable<string>;
  userRole!: string;


  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(
    private http: HttpClient,
    private notification: NotificationService,
    private _liveAnnouncer: LiveAnnouncer,
    private store: Store<ProfileInterface>,
    private mainStore: Store<Loading>,
    private LoadAPI: LoadAPIService,
    private dialog: DialogService,
    ) {
    this.role$ = store.select(getUserRole)
    this.role$.subscribe((role) => {
      this.userRole = role;
    })
    this.load$ = mainStore.select(getLoadingStatus);
    this.load$.subscribe((status) => {
      this.isLoading = status;
    })
    this.mainStore.dispatch(LoadingOn());
  }

  ngOnInit(): void {
    this.LoadAPI.getLoads().subscribe({
      next: (res) => {
        this.data = res.loads.reverse()
        this.setTable();
        this.mainStore.dispatch(LoadingOff());
      },
      error: () => {
        this.notification.openSnackBar('We cannot load your loads');
        setTimeout(() => this.notification.closeSnackBar(), 4000);
        this.mainStore.dispatch(LoadingOff());
      }
    })
  }

  handlePageEvent(event: PageEvent) {
    this.length = event.length;
    this.pageSize = event.pageSize;
    this.pageIndex = event.pageIndex;
  }

  openDialog(): void {
    const dialogRef = this.dialog.openDialog(LoadsModalComponent, '1000px');

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.mainStore.dispatch(LoadingOn());
        this.LoadAPI.addLoad( {
          name: result.name,
          payload: result.payload,
          pickup_address: result.pickup_address,
          delivery_address: result.delivery_address,
          width: result.dimensions.width,
          length: result.dimensions.length,
          height: result.dimensions.height,
          created_date: result.created_date,
        }).subscribe({
          next: (res) => {
            if (res.status === 200) {
              this.data.unshift(res.load)
              this.setTable();
              this.notification.openSnackBar('We successfully create your load');
              setTimeout(() => this.notification.closeSnackBar(), 4000);
              this.mainStore.dispatch(LoadingOff());
            }
          },
          error: () => {
            this.notification.openSnackBar('We cannot create your load');
            setTimeout(() => this.notification.closeSnackBar(), 4000);
            this.mainStore.dispatch(LoadingOff());
          }
        })
      }
    })
  }

  openLoad(load: Load): void {
    const dialogRef = this.dialog.openDialogWithData( ItemModalComponent, load, '600px');

    dialogRef.afterClosed().subscribe((result) => {
      if(result === 'POST') {
        this.mainStore.dispatch(LoadingOn());
        this.LoadAPI.postLoad(load._id).subscribe({
          next: (res) => {
            if (res.status === 200) {
              load.status = 'ASSIGNED'
              this.notification.openSnackBar('Driver was found');
              setTimeout(() => this.notification.closeSnackBar(), 4000);
              this.mainStore.dispatch(LoadingOff());
            }
          },
          error: () => {
            this.notification.openSnackBar('We cannot find driver');
            setTimeout(() => this.notification.closeSnackBar(), 4000);
            this.mainStore.dispatch(LoadingOff());
          }
        })
      }
      if (result === 'DELETE') {
        this.mainStore.dispatch(LoadingOn());
        this.LoadAPI.deleteLoad(load._id).subscribe({
          next: (res) => {
            if (res.status === 200) {
              const index = this.data.indexOf(load)
              this.data.splice(index,1);
              this.setTable();
              this.notification.openSnackBar('Load was successfully deleted');
              setTimeout(() => this.notification.closeSnackBar(), 4000);
              this.mainStore.dispatch(LoadingOff());
            }
          },
          error: (err) => {
            this.notification.openSnackBar(err.error.message);
            setTimeout(() => this.notification.closeSnackBar(), 4000);
            this.mainStore.dispatch(LoadingOff());
          }
        })
      }
    })
  }

  announceSortChange(sortState: Sort) {
    if (sortState.direction) {
      this._liveAnnouncer.announce(`Sorted ${sortState.direction}ending`);
    } else {
      this._liveAnnouncer.announce('Sorting cleared');
    }
  }

  editLoad(event: MouseEvent) {
    event.stopPropagation();
    const editRef = this.dialog.openDialog( LoadsModalComponent, '1000px')

    editRef.afterClosed().subscribe((result: { old: Load; new: Load }) => {
      if(result) {
        const index = this.data.indexOf(result.old)
        this.data[index] = result.new;
        this.setTable()
        this.LoadAPI.updateLoad(result.new._id, result.new).subscribe({
          next: (res) => {
            if (res.status === 200) {
              this.notification.openSnackBar('Load details changed successfully')
              setTimeout(() => this.notification.closeSnackBar(), 4000);
            }
          },
          error: () => {
            this.notification.openSnackBar('We didn\'t update your load')
            setTimeout(() => this.notification.closeSnackBar(), 4000);
          }
        })
      }
    })
  }

  setTable() {
    this.length = this.data.length;
    this.loads = new MatTableDataSource<TableElementInterface>(this.data);
    this.loads.paginator = this.paginator;
    this.loads.sort = this.sort;
  }
}
