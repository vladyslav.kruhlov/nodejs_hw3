import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Component, Inject } from '@angular/core';
import { GeolocationService } from '@ng-web-apis/geolocation';
import { MapDirectionsService, MapGeocoder } from '@angular/google-maps';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Coordinates } from '../../../interfaces';

@Component({
  selector: 'app-map-modal',
  templateUrl: './map-modal.component.html',
  styleUrls: ['./map-modal.component.scss'],
})
export class MapModalComponent {
  center: google.maps.LatLngLiteral = { lat: 12, lng: 4 };
  zoom = 15;
  markerOptions: google.maps.MarkerOptions = { draggable: false };
  markerPositions: google.maps.LatLngLiteral[] = [];
  destination!: Coordinates;
  origin!: Coordinates;
  request!: google.maps.DirectionsRequest;
  directionsResults$!: Observable<google.maps.DirectionsResult | undefined>;

  constructor(
    private readonly geolocation$: GeolocationService,
    @Inject(MAT_DIALOG_DATA) public address: { delivery: string; pickUp: string; },
    private geocoder: MapGeocoder,
    private mapDirectionsService: MapDirectionsService,
    private mapRef: MatDialogRef<MapModalComponent>,
    ) {
    if (this.address !== null) {
      this.geocoder.geocode({ address: this.address.delivery })
        .subscribe((destination) => {
          console.log(destination.results[0], '1111111111111');
        if (destination.results[0]) {
          this.geocoder.geocode( {
            address: this.address.pickUp
          } ).subscribe( ( origin ) => {
            console.log(origin.results[0], '222222222222222');
            if (origin.results[0]) {
              const request: google.maps.DirectionsRequest = {
                destination: {
                  lat: destination.results[0].geometry.location.lat(),
                  lng: destination.results[0].geometry.location.lng(),
                },
                origin: {
                  lat: origin.results[0].geometry.location.lat(),
                  lng: origin.results[0].geometry.location.lng(),
                },
                travelMode: google.maps.TravelMode.DRIVING,
              };
              this.directionsResults$ = mapDirectionsService.route(request).pipe(map(response => response.result));
            }
          } );
        }
      })
      return;
    }

    this.geolocation$.subscribe((position) => {
      this.center = {
        lat: position.coords.latitude,
        lng: position.coords.longitude,
      };
    })
  }

  addMarker(event: google.maps.MapMouseEvent) {
    if (this.address) {
      return;
    }
    this.markerPositions.push(event.latLng!.toJSON());

    this.geocoder.geocode({
      location: this.markerPositions[0],
    }).subscribe((response) => {
      this.mapRef.close(response.results[0].formatted_address)
    })
  }
}
