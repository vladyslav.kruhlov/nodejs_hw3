import { FormControl, FormGroup } from '@angular/forms';
import { Component, ElementRef, Inject, ViewChild } from '@angular/core';
import { MapModalComponent } from '../map-modal/map-modal.component';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Load } from '../../../interfaces/loads';
import { DialogService } from '../../services/dialog.service';

@Component({
  selector: 'app-loads-modal',
  templateUrl: './loads-modal.component.html',
  styleUrls: ['./loads-modal.component.scss']
})
export class LoadsModalComponent {
  @ViewChild('pickUp') pickUp!: ElementRef;

  addLoadForm = new FormGroup({
    name: new FormControl<string>(this.item ? this.item.name : ''),
    payload: new FormControl<number|null>(this.item ? this.item.payload : null),
    pickup_address: new FormControl<string>(this.item ? this.item.pickup_address : ''),
    delivery_address: new FormControl<string>(this.item ? this.item.delivery_address : ''),
    width: new FormControl<number|null>(this.item ? this.item.dimensions.width : null),
    length: new FormControl<number|null>(this.item ? this.item.dimensions.length : null),
    height: new FormControl<number|null>(this.item ? this.item.dimensions.height : null),
  })

  constructor(
    private dialogRef: MatDialogRef<LoadsModalComponent>,
    @Inject(MAT_DIALOG_DATA) public item: Load,
    private map: DialogService,
  ) { }

  onSubmit(): void {
    this.dialogRef.close({
      name: this.addLoadForm.value.name,
      payload: this.addLoadForm.value.payload,
      pickup_address: this.addLoadForm.value.pickup_address,
      delivery_address: this.addLoadForm.value.delivery_address,
      dimensions: {
        width: this.addLoadForm.value.width,
        length: this.addLoadForm.value.length,
        height: this.addLoadForm.value.length,
      },
      created_date: new Date(),
      status: 'NEW',
    });
  }

  updateLoad() {
    this.dialogRef.close({
      old: this.item,
      new: {
        _id: this.item._id,
        created_by: this.item.created_by,
        name: this.addLoadForm.value.name!,
        payload: this.addLoadForm.value.payload!,
        pickup_address: this.addLoadForm.value.pickup_address!,
        delivery_address: this.addLoadForm.value.delivery_address!,
        dimensions: {
          width: this.addLoadForm.value.width!,
          length: this.addLoadForm.value.length!,
          height: this.addLoadForm.value.height!,
        },
        status: 'NEW',
        created_date: this.item.created_date,
        logs: [],
      }
    });
  }

  openMap(inputField: string): void {
    const mapRef = this.map.openDialog( MapModalComponent, '600px');

    mapRef.afterClosed().subscribe((address) => {
      if (address) {
        if(inputField === 'PICKUP') {
          this.addLoadForm.patchValue({
            pickup_address: address
          })
        } else if (inputField === 'DELIVERY') {
          this.addLoadForm.patchValue({
            delivery_address: address
          })
        }
      }
    })
  }
}
