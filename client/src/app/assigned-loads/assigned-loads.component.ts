import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { getLoadingStatus } from '../reducers';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Load } from '../../interfaces/loads';
import { MatSort } from '@angular/material/sort';
import { MatDialog } from '@angular/material/dialog';
import { TableElementInterface } from '../../interfaces';
import { DialogService } from '../services/dialog.service';
import { MatTableDataSource } from '@angular/material/table';
import { LoadAPIService } from '../services/LoadAPI.service';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { NotificationService } from '../services/notification.service';
import { Loading } from '../reducers/main/main.reducer';
import { LoadingOff, LoadingOn } from '../reducers/main/main.actions';
import { ItemModalComponent } from '../new-loads/item-modal/item-modal.component';


@Component( {
  selector: 'app-assigned-loads',
  templateUrl: './assigned-loads.component.html',
  styleUrls: ['./assigned-loads.component.scss']
} )
export class AssignedLoadsComponent implements OnInit {
  @ViewChild( MatPaginator ) paginator!: MatPaginator;
  @ViewChild( MatSort ) sort!: MatSort;

  loads!: MatTableDataSource<TableElementInterface>;
  displayedColumns: string[] = ['index', 'name', 'date', 'status', 'pickup', 'delivery'];
  length!: number;
  pageSize: number = 10;
  pageSizeOptions: number[] = [10, 20, 30, 40, 50];
  pageIndex = 1;
  load$!: Observable<boolean>;
  isLoading: boolean = false;

  constructor(
    private dialog: MatDialog,
    private notification: NotificationService,
    private LoadAPI: LoadAPIService,
    private modal: DialogService,
    private store: Store<Loading>,
  ) {
    this.load$ = this.store.select(getLoadingStatus);
    this.load$.subscribe((status) => {
      this.isLoading = status;
    });
    store.dispatch(LoadingOn());
  }

  ngOnInit(): void {
    this.LoadAPI.getLoads().subscribe( {
      next: ( res ) => {
        const data = res.loads.filter( ( load: Load ) => load.status === 'ASSIGNED' ).reverse();
        this.length = data.length;
        this.loads = new MatTableDataSource<TableElementInterface>( data );
        this.loads.paginator = this.paginator;
        this.loads.sort = this.sort;
        this.store.dispatch(LoadingOff());
      },
      error: () => {
        this.notification.openSnackBar( 'We cannot load your loads' )
        setTimeout( () => this.notification.closeSnackBar(), 4000 );
        this.store.dispatch(LoadingOff());
      }
    } )
  }

  openLoad( load: Load ): void {
    this.modal.openDialogWithData(ItemModalComponent, load, '600px');
  }

  handlePageEvent( event: PageEvent ) {
    this.length = event.length;
    this.pageSize = event.pageSize;
    this.pageIndex = event.pageIndex;
  }
}
