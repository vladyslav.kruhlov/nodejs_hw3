import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { getLoadingStatus } from '../../reducers';
import { MatDialogRef } from '@angular/material/dialog';
import { UserAPIService } from '../../services/UserAPI.service';
import { NotificationService } from '../../services/notification.service';
import { Loading } from '../../reducers/main/main.reducer';
import { LoadingOff, LoadingOn } from '../../reducers/main/main.actions';

@Component({
  selector: 'app-password-modal',
  templateUrl: './password-modal.component.html',
  styleUrls: ['./password-modal.component.scss']
})
export class PasswordModalComponent {
  passwordForm = new FormGroup(({
    oldPassword: new FormControl(''),
    newPassword: new FormControl(''),
    confirmPassword: new FormControl(''),
  }))
  load$!: Observable<boolean>;
  isLoading = false;

  constructor(
    private notification: NotificationService,
    private dialogRef: MatDialogRef<PasswordModalComponent>,
    private store: Store<Loading>,
    private UserAPI: UserAPIService,
    ) {
    this.load$ = store.select(getLoadingStatus)
    this.load$.subscribe((status) => {
      this.isLoading = status
    })
  }

  onSubmit(): void {
    const { oldPassword, newPassword, confirmPassword } = this.passwordForm.value;

    if (newPassword !== confirmPassword) {
      this.notification.openSnackBar('Passwords don\'t match')
      setTimeout(() => this.notification.closeSnackBar(), 4000);
      return;
    }

    this.store.dispatch(LoadingOn());
    this.UserAPI.changePassword(oldPassword!, newPassword!).subscribe({
      next: (res) => {
        if (res.status === 200) {
          this.notification.openSnackBar('Password successfully changes');
          setTimeout(() => this.notification.closeSnackBar(), 4000);
          this.dialogRef.close();
          this.store.dispatch(LoadingOff());
        }
      },
      error: () => {
        this.store.dispatch(LoadingOff());
        this.notification.openSnackBar('We couldn\'t change your password');
        setTimeout(() => this.notification.closeSnackBar(), 4000);
      }
    })
  }
}
