import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { getLoadingStatus, getProfile } from '../reducers';
import { HttpClient } from '@angular/common/http';
import { DialogService } from '../services/dialog.service';
import { UserAPIService } from '../services/UserAPI.service';
import { ImageAPIService } from '../services/ImageAPI.service';
import { NotificationService } from '../services/notification.service';
import { DeleteModalComponent } from './delete-modal/delete-modal.component';
import { PasswordModalComponent } from './password-modal/password-modal.component';
import { LoadingOff, LoadingOn } from '../reducers/main/main.actions';
import { ProfileInterface } from '../reducers/profile/profile.reducer';

@Component( {
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
} )
export class ProfileComponent implements OnInit {

  imageForm = new FormGroup( {
    image: new FormControl(),
  } )
  photo!: string;
  defaultPhoto = '../../assets/images.png';
  profile$!: Observable<ProfileInterface>;
  user!: ProfileInterface;
  load$!: Observable<boolean>;
  isLoading: boolean = false;

  constructor(
    private http: HttpClient,
    private notification: NotificationService,
    private dialog: DialogService,
    private router: Router,
    private store: Store<ProfileInterface>,
    private ImageAPI: ImageAPIService,
    private UsersAPI: UserAPIService,
  ) {
    this.profile$ = store.select( getProfile );
    this.profile$.subscribe( ( info ) => {
      this.user = { ...info };
    } )
    this.load$ = store.select(getLoadingStatus);
    this.load$.subscribe((status) => {
      this.isLoading = status;
    })
  }

  ngOnInit(): void {
    this.ImageAPI.getProfileImage().subscribe( ( res ) => {
        this.photo = res.image.toString( 'base64' );
      }
    )
  }

  openPassword(): void {
    this.dialog.openDialog( PasswordModalComponent, '500px' );
  }

  openDelete(): void {
    const dialogRef = this.dialog.openDialog( DeleteModalComponent, '300px' );

    dialogRef.afterClosed().subscribe( ( res ) => {
      if (res) {
        this.store.dispatch( LoadingOn() );
        this.UsersAPI.deleteUser().subscribe( {
          next: ( result ) => {
            if (result.status === 200) {
              localStorage.removeItem( 'jwt-token' );
              this.notification.openSnackBar( 'Your account successfully deleted' );
              setTimeout( () => this.notification.closeSnackBar(), 4000 );
              this.router.navigate( ['login'] );
              this.store.dispatch( LoadingOff() );
            }
          },
          error: () => {
            this.notification.openSnackBar( 'We cannot delete your profile' );
            setTimeout( () => this.notification.closeSnackBar(), 4000 );
            this.store.dispatch( LoadingOff() );
          }
        } )
      }
    } )
  }

  loadImage( event: any ): void {
    event.stopPropagation();
    if (event.target && !event.target[0].files[0]) {
      this.notification.openSnackBar( 'Choose photo before upload it' );
      setTimeout( () => this.notification.closeSnackBar(), 4000 );
      return;
    }
    const reader = new FileReader();

    reader.readAsDataURL( event.target[0].files[0] );
    reader.onload = () => {
      this.store.dispatch( LoadingOn() );
      if (typeof reader.result === 'string') {
        this.ImageAPI.updateImage( reader.result ).subscribe( {
          next: () => {
            this.store.dispatch( LoadingOff() );
            if(typeof reader.result === 'string') {
              this.photo = reader.result;
            }
            this.notification.openSnackBar( 'Image successfully uploaded' );
            setTimeout( () => this.notification.closeSnackBar(), 4000 );
          },
          error: (err) => {
            this.notification.openSnackBar( 'We cannot upload your photo' );
            setTimeout( () => this.notification.closeSnackBar(), 4000 );
            this.store.dispatch( LoadingOff() );
          }
        } )
      }
    };
    reader.onerror = () => {
      this.notification.openSnackBar( 'We cannot upload your photo' );
      setTimeout( () => this.notification.closeSnackBar(), 4000 );
    };
  }
}
