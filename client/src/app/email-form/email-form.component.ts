import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { AuthAPIService } from '../services/AuthAPI.service';
import { NotificationService } from '../services/notification.service';

@Component( {
  selector: 'app-email-form',
  templateUrl: './email-form.component.html',
  styleUrls: ['./email-form.component.scss']
} )
export class EmailFormComponent {
  resetGroup = new FormGroup( {
    email: new FormControl( '' ),
    newPassword: new FormControl( '' ),
    confirmPassword: new FormControl( '' ),
  } )

  constructor(
    private AuthAPI: AuthAPIService,
    private notification: NotificationService,
  ) {}

  onSubmit() {
    const { newPassword, confirmPassword, email } = this.resetGroup.value;

    if (newPassword !== confirmPassword) {
      this.notification.openSnackBar( 'Your password doesn\'t match' )
      setTimeout( () => this.notification.closeSnackBar(), 4000 );
      return;
    }
    this.AuthAPI.resetPassword( newPassword!, email! ).subscribe( {
      next: ( res ) => {
        if (res.status === 200) {
          this.notification.openSnackBar( 'Password reset successfully!' )
          setTimeout( () => this.notification.closeSnackBar(), 4000 );
        }
      },
      error: ( error ) => {
        this.notification.openSnackBar( error.error.message || 'Something went wrong' )
        setTimeout( () => this.notification.closeSnackBar(), 4000 );
      }
    } )
  }
}
