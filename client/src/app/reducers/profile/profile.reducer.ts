import { createReducer, on } from '@ngrx/store';
import { setUserInfo } from './profile.actions';

export interface Chat {
  id: string;
  friend_name: string;
  load: string;
}

export interface ProfileInterface {
  _id: string;
  role: string;
  email: string;
  createdDate: string;
  name: string;
  chats: Chat[];
}

export const initialState: ProfileInterface = {
  _id: '',
  role: '',
  email: '',
  createdDate: '',
  name: '',
  chats: [],
};

export const profileReducer = createReducer(
  initialState,
  on( setUserInfo, ( state, { _id, role, email, createdDate, name, chats } ) => {
    return {
      _id,
      role,
      email,
      createdDate,
      name,
      chats,
    }
  } ),
);

export const getAllInfo = (state: ProfileInterface) => state;
export const getRole = (state: ProfileInterface) => state.role;
export const getId = (state: ProfileInterface) => state._id;
export const getName = (state: ProfileInterface) => state.name;
export const getEmail = (state: ProfileInterface) => state.email;
export const getChats = (state: ProfileInterface) => state.chats;
