import { createAction, props } from '@ngrx/store';
import { ProfileInterface } from './profile.reducer';

export const setUserInfo = createAction('[Main Page] SetUserInfo', props<ProfileInterface>());
