import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer
} from '@ngrx/store';
import { environment } from '../../environments/environment';
import {
  getAllInfo,
  getChats,
  getId, getName,
  getRole,
  ProfileInterface,
  profileReducer
} from './profile/profile.reducer';
import { getLoading, Loading, mainReducer } from './main/main.reducer';

export interface State {}

export const reducers: ActionReducerMap<State> = {
  profile: profileReducer,
  main: mainReducer,
};

export const getProfileState =
  createFeatureSelector<ProfileInterface>('profile');

export const getMainState =
  createFeatureSelector<Loading>('main');

export const getUserRole =
  createSelector(getProfileState, getRole)

export const getUserId =
  createSelector(getProfileState, getId);

export const getProfile =
  createSelector(getProfileState, getAllInfo);

export const getUserName =
  createSelector(getProfileState, getName);

export const getUserChats =
  createSelector(getProfileState, getChats);

export const getLoadingStatus =
  createSelector(getMainState, getLoading);


export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : [];
