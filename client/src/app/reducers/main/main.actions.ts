import { createAction } from '@ngrx/store';

export const LoadingOn = createAction('[Main Page] LoadingOn');
export const LoadingOff = createAction('[Main Page] LoadingOff');
