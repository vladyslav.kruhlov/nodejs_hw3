import { createReducer, on } from '@ngrx/store';
import { LoadingOn, LoadingOff } from './main.actions';

export interface Loading {
  isLoading: boolean;
}

export const initialState: Loading = {
  isLoading: false,
};

export const mainReducer = createReducer(
  initialState,
  on(LoadingOn, () => ({ isLoading: true }) ),
  on(LoadingOff, () => ({ isLoading: false }) ),
);

export const getLoading = (state: Loading) => state.isLoading;
