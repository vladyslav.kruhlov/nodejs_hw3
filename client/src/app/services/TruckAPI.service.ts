import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PostTruckResponse, TruckResponse } from '../../interfaces/trucks';
import { SimpleResponse } from '../../interfaces';

@Injectable({
  providedIn: 'root'
})

export class TruckAPIService {
  private baseURL: string = 'http://localhost:8080/api/trucks';

  constructor(private _http: HttpClient) {
  }

  getTruck(): Observable<TruckResponse> {
    return this._http.get<TruckResponse>(this.baseURL, {
      headers: new HttpHeaders( {
        'Authorization': 'Bearer ' + localStorage.getItem( 'jwt-token' )
      }),
    })
  }

  assignTruck(id: string): Observable<SimpleResponse> {
    return this._http.post<SimpleResponse>(`${this.baseURL}/${id}/assign`, null, {
      headers: new HttpHeaders( {
        'Authorization': 'Bearer ' + localStorage.getItem( 'jwt-token' )
      }),
    })
  }

  deleteTruck(id: string): Observable<SimpleResponse> {
    return this._http.delete<SimpleResponse>(`${this.baseURL}/${id}`, {
      headers: new HttpHeaders( {
        'Authorization': 'Bearer ' + localStorage.getItem( 'jwt-token' )
      }),
    })
  }

  addTruck(type: string): Observable<PostTruckResponse> {
    return this._http.post<PostTruckResponse>(this.baseURL, { type }, {
      headers: new HttpHeaders( {
        'Authorization': 'Bearer ' + localStorage.getItem( 'jwt-token' )
      }),
    })
  }

  updateTruckInfo(id: string, type: string): Observable<PostTruckResponse> {
    return this._http.put<PostTruckResponse>(`${this.baseURL}/${id}`, { type }, {
      headers: new HttpHeaders( {
        'Authorization': 'Bearer ' + localStorage.getItem( 'jwt-token' )
      }),
    })
  }
}
