import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ResponseUser } from '../../interfaces/users';
import { SimpleResponse } from '../../interfaces';

@Injectable( {
  providedIn: 'root'
} )

export class UserAPIService {
  private baseURL: string = 'http://localhost:8080/api/users';

  constructor(private _http: HttpClient) {}

  getUserInfo(): Observable<ResponseUser> {
    return this._http.get<ResponseUser>(`${this.baseURL}/me`, {
      headers: new HttpHeaders( {
        'Authorization': 'Bearer ' + localStorage.getItem( 'jwt-token' )
      }),
    })
  }

  deleteUser(): Observable<SimpleResponse> {
    return this._http.delete<SimpleResponse>(`${this.baseURL}/me`, {
      headers: new HttpHeaders( {
        'Authorization': 'Bearer ' + localStorage.getItem( 'jwt-token' )
      }),
    })
  }

  changePassword(oldPassword: string, newPassword: string): Observable<SimpleResponse> {
    return this._http.patch<SimpleResponse>(`${this.baseURL}/me/password`,{ oldPassword, newPassword }, {
      headers: new HttpHeaders( {
        'Authorization': 'Bearer ' + localStorage.getItem( 'jwt-token' )
      }),
    })
  }
}
