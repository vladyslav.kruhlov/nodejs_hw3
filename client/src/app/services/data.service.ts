import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Chat } from '../reducers/profile/profile.reducer';

@Injectable({
  providedIn: 'root'
})

export class DataService {
  data$: Observable<Chat> = of({
    id: '',
    friend_name: '',
    load: '',
  })

  sendData(user: Chat) {
    this.data$ = of(user);
    this.data$.subscribe((data) => {
    })
  }

  getData() {
    return this.data$
  }
}
