import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';

import { Observable } from 'rxjs';
import { Chat } from '../reducers/profile/profile.reducer';
import { DataService } from './data.service';

@Injectable({
  providedIn: 'root'
})
export class NewsResolver implements Resolve<Observable<Chat>> {
  constructor( private ds: DataService ) {
  }

  resolve(): Observable<Chat> {
    return this.ds.getData()
  }

}

