import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ComponentType } from '@angular/cdk/overlay';
import { Load } from '../../interfaces/loads';
import { Truck } from '../../interfaces/trucks';
import { MapDataInterface } from '../../interfaces';

@Injectable( {
  providedIn: 'root'
} )

export class DialogService {

  constructor( private dialog: MatDialog ) {
  }

  openDialog(component: ComponentType<any>, width: string) {
    return this.dialog.open( component, {
      width,
      enterAnimationDuration: '0ms',
      exitAnimationDuration: '0ms',
    } );
  }

  openDialogWithData( component: ComponentType<any>, data: Load | Truck | MapDataInterface, width: string ) {
    return this.dialog.open( component, {
      width,
      enterAnimationDuration: '0ms',
      exitAnimationDuration: '0ms',
      data,
    } );
  }
}
