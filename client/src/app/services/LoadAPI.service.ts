import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import {
  AddLoadResponse, GetActiveLoadInterface, IterateResponse,
  Load,
  LoadGetAllInterface,
  LoadPostInterface,
  PostLoadInterface
} from '../../interfaces/loads';
import { SimpleResponse } from '../../interfaces';

@Injectable({
  providedIn: 'root'
})

export class LoadAPIService {
  private baseURL: string = 'http://localhost:8080/api/loads'

  constructor( private _http: HttpClient ) { }

  getLoads(): Observable<LoadGetAllInterface> {
    return this._http.get<LoadGetAllInterface>(this.baseURL, {
      headers: new HttpHeaders( {
        'Authorization': 'Bearer ' + localStorage.getItem( 'jwt-token' )
      }),
    })
  }

  addLoad( { name, payload, pickup_address, delivery_address, width, length, height, created_date }: LoadPostInterface ): Observable<AddLoadResponse> {
    return this._http.post<AddLoadResponse>(this.baseURL, {
      name,
      payload,
      pickup_address,
      delivery_address,
      dimensions: {
        width,
        length,
        height,
      },
      created_date,
    }, { headers: new HttpHeaders( {
        'Authorization': 'Bearer ' + localStorage.getItem( 'jwt-token' )
      }), })
  }

  postLoad(id: string): Observable<PostLoadInterface> {
    return this._http.post<PostLoadInterface>(`${this.baseURL}/${id}/post`, null, {
      headers: new HttpHeaders( {
        'Authorization': 'Bearer ' + localStorage.getItem( 'jwt-token' )
      }),
    })
  }

  deleteLoad(id: string): Observable<SimpleResponse> {
    return this._http.delete<SimpleResponse>(`${this.baseURL}/${id}`, {
      headers: new HttpHeaders( {
        'Authorization': 'Bearer ' + localStorage.getItem( 'jwt-token' )
      }),
    })
  }

  updateLoad(id: string, newLoad: Load): Observable<SimpleResponse> {
    return this._http.put<SimpleResponse>(`${this.baseURL}/${id}`, newLoad , {
      headers: new HttpHeaders( {
        'Authorization': 'Bearer ' + localStorage.getItem( 'jwt-token' )
      }),
    })
  }

  iterateLoadStatus(): Observable<IterateResponse> {
    return this._http.patch<IterateResponse>(`${this.baseURL}/active/state`, null, {
      headers: new HttpHeaders( {
        'Authorization': 'Bearer ' + localStorage.getItem( 'jwt-token' )
      }),
    })
  }

  getActiveLoad(): Observable<GetActiveLoadInterface> {
    return this._http.get<GetActiveLoadInterface>(`${this.baseURL}/active`, {
      headers: new HttpHeaders( {
        'Authorization': 'Bearer ' + localStorage.getItem( 'jwt-token' )
      }),
    })
  }
}
