import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SimpleResponse } from '../../interfaces';
import { NotificationService } from './notification.service';
import { Router } from '@angular/router';
import { Loading } from '../reducers/main/main.reducer';
import { Store } from '@ngrx/store';
import { LoadingOn } from '../reducers/main/main.actions';
import { AuthLoginInterface } from '../../interfaces/auth';

@Injectable( {
  providedIn: 'root'
} )

export class AuthAPIService {
  private baseURL = 'http://localhost:8080/api/auth';

  constructor(
    private _http: HttpClient,
    private notification: NotificationService,
    private router: Router,
    private store: Store<Loading>,
  ) {
  }

  resetPassword( newPassword: string, email: string ): Observable<SimpleResponse> {
    return this._http.put<SimpleResponse>( `${this.baseURL}/resetPassword`, { newPassword, email } );
  }

  forgotPassword( email: string ): Observable<SimpleResponse> {
    return this._http.post<SimpleResponse>( `${this.baseURL}/forgotPassword`, { email } )
  }

  login( email: string, password: string ): Observable<AuthLoginInterface> {
    this.store.dispatch( LoadingOn() );
    return this._http.post<AuthLoginInterface>( `${this.baseURL}/login`, { email, password } )
  }

  register(email: string, role: string, password: string, name: string): Observable<any> {
    return this._http.post(`${this.baseURL}/register`, { email, role, password, name, })
  }
}
