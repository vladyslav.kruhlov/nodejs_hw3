import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { Observable, of } from 'rxjs';
import { SocketUser } from '../../interfaces/socket';
import { SendMessage } from '../../interfaces';

@Injectable({
  providedIn: 'root'
})
export class SocketService {
  observableUsers$: Observable<SocketUser[]> = of([]);

  constructor(private socket: Socket) { }

  getSocket() {
    return this.socket;
  }

  getSocketUsers() {
    return this.observableUsers$;
  }

  connect(username: string ) {
    this.socket.ioSocket.auth = { username }
    this.socket.connect()
  }

  disconnect() {
    this.socket.disconnect();
  }

  sendMessage({content, chatID, from, userID }: SendMessage) {
    this.socket.emit('chat message', { content, chatID, from, userID })
  }
}
