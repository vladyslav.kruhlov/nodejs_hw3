import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SimpleResponse } from '../../interfaces';

@Injectable({
  providedIn: 'root'
})

export class ImageAPIService {
  private baseURL = 'http://localhost:8080/api/image';

  constructor(private _http: HttpClient) {}

  getProfileImage(): Observable<any> {
    return this._http.get<Response>(this.baseURL, {
      headers: new HttpHeaders( {
        'Authorization': 'Bearer ' + localStorage.getItem( 'jwt-token' )
      }),
    });
  }

  updateImage(file: string): Observable<SimpleResponse> {
    return this._http.post<SimpleResponse>(`${this.baseURL}/upload`, { file }, {
      headers: new HttpHeaders( {
        'Authorization': 'Bearer ' + localStorage.getItem( 'jwt-token' )
      }), })
  }
}
