import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { getLoadingStatus, getProfile, } from '../reducers';
import { UserAPIService } from '../services/UserAPI.service';
import { NotificationService } from '../services/notification.service';
import { Loading } from '../reducers/main/main.reducer';
import { setUserInfo } from '../reducers/profile/profile.actions';
import { LoadingOff, LoadingOn } from '../reducers/main/main.actions';
import { ProfileInterface } from '../reducers/profile/profile.reducer';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss'],
})
export class MainPageComponent implements OnInit {
  load$!: Observable<boolean>;
  isLoading = false;
  info!: Observable<ProfileInterface>;
  userInfo!: ProfileInterface;

  constructor(
    private router: Router,
    private store: Store<ProfileInterface>,
    private notification: NotificationService,
    private mainStore: Store<Loading>,
    private UserAPI: UserAPIService,
    ) {
    this.load$ = mainStore.select(getLoadingStatus)
    this.load$.subscribe((status) => {
      this.isLoading = status;
    })
  }

  ngOnInit(): void {
    if (!localStorage.getItem('jwt-token')) {
      this.router.navigate(['/login']);
    }
    this.mainStore.dispatch(LoadingOn());
    this.UserAPI.getUserInfo().subscribe({
      next: (res) => {
        if(res.user) {
          console.log(res.user)
          this.store.dispatch(setUserInfo(res.user));
          this.mainStore.dispatch(LoadingOff());
        }
      },
      error: (err) => {
        if (err.status === 401) {
          localStorage.removeItem('jwt-token');
          this.router.navigate(['/login']);
        }
        this.mainStore.dispatch(LoadingOff());
        this.notification.openSnackBar('We cannot get your profile info');
        setTimeout(() => this.notification.closeSnackBar(), 4000);
      }
    })
    this.info = this.store.select(getProfile);
    this.info.subscribe((info) => {
      this.userInfo = info;
    })
  }

  logout(): void {
    this.mainStore.dispatch(LoadingOn());
    localStorage.removeItem('jwt-token');
    setTimeout(() => {
      this.mainStore.dispatch(LoadingOff());
      this.router.navigate(['/login']);
    }, 1000);
  }
}
