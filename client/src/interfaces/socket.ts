export interface SocketUser {
  self: any;
  username: number;
  userID: string;
  clientID: string;
}
