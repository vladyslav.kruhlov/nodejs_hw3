import { SimpleResponse } from './index';

export interface AuthLoginInterface extends SimpleResponse{
  jwt_token: string;
}
