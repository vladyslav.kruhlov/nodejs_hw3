import { SimpleResponse } from './index';

export interface Load {
  _id: string;
  created_by: string;
  status: string;
  dimensions: {
    width: number;
    height: number;
    length: number;
  };
  payload: number;
  pickup_address: string;
  delivery_address: string;
  logs: Logs[];
  state?: string;
  assigned_to?: string;
  name: string;
  created_date: string;
}

export interface IterateResponse extends SimpleResponse {
  state: string;
}

export interface AddLoadResponse extends SimpleResponse {
  load: Load,
}

interface Logs extends SimpleResponse {
  time: string;
}

export interface LoadGetAllInterface {
  loads: Load[];
  message: string;
  status: number;
  total_count: number;
}

export interface LoadPostInterface {
  name: string;
  payload: number,
  pickup_address: string,
  delivery_address: string,
  width: number,
  length: number,
  height: number,
  created_date: string,
}

export interface PostLoadInterface extends SimpleResponse {
  driver_found: boolean;
}

export interface GetActiveLoadInterface extends SimpleResponse {
  load: Load,
}
