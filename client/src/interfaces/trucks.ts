import { SimpleResponse } from './index';

export interface Truck {
  "_id": string;
  "created_by": string;
  "assigned_to": string;
  "type": string;
  "status": string;
  "created_date": string;
  payload_max: number;
  dimensions: {
    width: number;
    height: number;
    length: number;
  }
}

export interface PostTruckResponse extends SimpleResponse{
  truck: Truck;
}

export interface TruckResponse extends SimpleResponse {
  trucks: Truck[],
  total_count: number;
}
