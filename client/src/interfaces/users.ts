import { Chat } from '../app/reducers/profile/profile.reducer';

export interface User {
  id: string;
  email: string;
  role: string;
  createdDate: string;
}

export interface ResponseUser {
  user: {
    _id: string;
    email: string;
    role: string;
    createdDate: string;
    name: string;
    chats: Chat[];
  }
}
