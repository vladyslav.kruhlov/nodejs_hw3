export interface SimpleResponse {
  message: string;
  status: number;
}

export interface TableElementInterface {
  name: string;
  created_date: string;
  pickup_address: string;
  delivery_address: string;
  status: string;
}

export interface Coordinates {
  lat: number;
  lng: number;
}

export interface MapDataInterface {
  pickUp: string;
  delivery: string;
}

export interface MessageResponse {
  messages: {
    from: string;
    content: string;
    userID: string;
  }[];
  status: number;
}

export interface SendMessage {
  content: string;
  chatID: string;
  from: string;
  userID: string;
}


